angular.module('userModule')
    .controller('ConfigCitasCtrl', function($scope,MessageResource) {
        $(document).ready(function() {
            var year = new Date().getFullYear();
            var month = new Date().getMonth();
            var day = new Date().getDate();
            var nuevoMes =  parseInt(month.toString())+1;
            var fechaFinal = year+'-'+nuevoMes+'-'+day;
            var ur = "http://" + config.ipAdress + "/server-odonto/citasControl/" + fechaFinal + "/" + 1;

            $.ajax({
                type: "GET",
                url: ur,
                success: function OnGetMemberSuccess(data) {
                    config.listResult=data;
                    $('#calendar').weekCalendar({
                        data: data,
                        minDate:  new Date(),
                        maxDate: new Date(),
                        switchDisplay: {'Hoy': 1,'Semana': 5},
                        timeslotsPerHour: 4,
                        height: function($calendar) {
                            return $(window).height() - $('h1').outerHeight() - $('.description').outerHeight();
                        },
                        eventClick: function(calEvent, $event) {

                        },
                        eventRender: function(calEvent, $event) { },
                        eventDrop: function(calEvent, $event) {},
                        eventResize: function(calEvent, $event) {},
                        eventMouseover: function(calEvent, $event){},
                        eventMouseout: function(calEvent, $event) {},
                        noEvents: function() {},
                        reachedmindate: function($calendar, date) {},
                        reachedmaxdate: function($calendar, date) {}
                    });
                },
                error: function OnGetMemberSuccess(data, status) {
                    console.log(data);
                }
            });


        });
    });