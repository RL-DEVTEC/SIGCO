function Cita(id,fecha,horaI,horaF,persona) {
    this.id = id;
    this.fecha = fecha;
    this.horaI = horaI;
    this.horaF = horaF;
    this.persona=persona;
}
function consultarDisponibilidadCita(horaI,horaF,fecha) {
    var ur = "http://" + config.ipAdress + "/server-odonto/VPRE?horaI="+horaI+"&horaF="+horaF+"&fecha="+fecha;
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async:false,
        url: ur,
        success: function OnGetMemberSuccess(data) {
            config.mensaje =data;
        },
        error: function OnGetMemberSuccess(data, status) {
            console.log(data);
            config.mensaje ="error";
            // config.mensaje="Error en el servidor";
        }
    });
}
function cambiarEstadoCita(id,estado,descrip) {
    var ur = "http://" + config.ipAdress + "/server-odonto/CE?id="+id+"&estado="+estado+"&descrip="+descrip;
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        url: ur,
        success: function OnGetMemberSuccess(data) {


            config.mensaje ="Operación realizada satisfactoriamente";
        },
        error: function OnGetMemberSuccess(data, status) {
            //alert("Error");
            console.log(data);
            config.mensaje="Error en el servidor";
        }
    });
}
function changeObjects(){
    $("#input_est").val('');
    $("#input_Inh").val('');
    $('#label_stu').text('');

    if($('#add_select').val() == "asignar"){

        $('#input_est').val(" ");
        $('#label_stu').val(" ");
        $('#select_div').css('margin-left','60px'); //button_search
        $('#div_info').css('display','block');
        $('#label_stu').css('display','block');
        $('#button_search').css('display','block');

        $("#input_est").css('display','block');
        $("#input_Inh").css('display','none');

        //  $("#input_est").attr("placeholder", "Ingrese el carné").placeholder();


        // $('#input_est').attr('type', 'number');
    }
    else{
        $('#button_search').css('display','none');
        //$("#input_est").attr("placeholder", "Descripción").placeholder();
        $('#label_stu').css('display','none');
        $("#input_Inh").css('display','block');
        $("#input_est").css('display','none');
        //  $('#input_est').attr('type', 'text');

    }
}
function consultarDescCita(idCita){
    var ur = "http://" + config.ipAdress + "/"+config.server_name+"/GDC"+"/"+idCita;
    $.ajax({
        type: "GET",
        url: ur,
        async: false,
        success: function OnGetMemberSuccess(data) {
            config.returnVal=data;

        },
        error: function OnGetMemberSuccess(data, status) {
            console.log("Error");
            console.log(data);
            config.returnVal="Error al consultar...";
        }
    });
}
function consultEstAsignado(idCita){
    var ur = "http://" + config.ipAdress + "/"+config.server_name+"/CECA"+"/"+idCita;
    $.ajax({
        type: "GET",
        url: ur,
        async: false,
        success: function OnGetMemberSuccess(data) {
            config.returnVal=data;

        },
        error: function OnGetMemberSuccess(data, status) {
            config.returnVal="Error al consultar...";
        }
    });
}
function consultarEstado(idCita){
    var ur = "http://" + config.ipAdress + "/"+config.server_name+"/consultarEC"+"/"+idCita;

    $.ajax({
        type: "GET",
        url: ur,
        async: false,
        success: function OnGetMemberSuccess(data) {

            config.consult=data;

        },
        error: function OnGetMemberSuccess(data, status) {
            config.consult=3;
        }
    });
}
function cambioCarne(){
    var ur = "http://" + config.ipAdress + "/"+config.server_name+"/estudiantes";
    console.log(ur);
    $.ajax({
        type: "GET",
        url: ur,
        success: function OnGetMemberSuccess(data) {

            var find = 0;

            var dato = $('#input_est').val();

            for(var i=0;i<data.length;i++){
                if(data[i].carne == dato.toString()){
                    $('#label_stu').css('color','black');
                    find = 1;
                    $('#label_stu').text(data[i].nombre+" "+data[i].ape1+" "+data[i].ape2);
                    break;
                }
            }
            $('#label_stu').css('display','block');
            if(find == 0){
                $('#label_stu').text("No existe el estudiante");
                $('#label_stu').css('color','red');
            }
            // $('#label_stu').css('margin-top','-53px');


        },
        error: function OnGetMemberSuccess(data, status) {

            console.log(data);
            alert("Error en el servidor");
        }
    });
}
function getIDcitaInsertada(horaI,horaF,fecha){
    var ur = "http://" + config.ipAdress + "/"+config.server_name+"/GDE?horaI="+horaI+"&horaF="+horaF+"&fecha="+fecha;

    $.ajax({
        type: "GET",
        url: ur,
        async: false,
        success: function OnGetMemberSuccess(data) {

            config.consult=data;
        },
        error: function OnGetMemberSuccess(data, status) {
            alert("Verificar");
            console.log(data);
            config.consult = 0;
        }
    });
}
function creaNuevaCita(carne, fecha, horaI, horaF,tipo,preID,descr){

    var cita;
    cita = new Cita(preID,fecha,horaI,horaF,carne);
    config.listResult.push(cita);


    var ur = "http://" + config.ipAdress + "/"+config.server_name+"/insertNewDate?carne="+carne+"&fecha="+fecha+
        "&horaI="+horaI+"&horaF="+horaF+"&tipo="+tipo+"&preID="+preID+"&descr="+descr;
        console.log("HERE");
        console.log(ur);
        $.ajax({
            type: "POST",
            url: ur,
            async: false,
            success: function OnGetMemberSuccess(data) {
                config.anterior=0;
                if(preID!=0){
                    var eventId=preID
                    //*************************
                    var cont=0; //$('.wc-time').each(function() {
                    $('.wc-cal-event').each(function() {
                        if ($(this).data('calEvent').id === eventId) {
                            $(this).css('backgroundColor', '#68a1e5');
                            updateTitleFromDate(eventId,cont);


                            return false;
                        }
                        cont+=1;
                    });
                }
                return 1;
                },
            error: function OnGetMemberSuccess(data, status) {
                    //alert("Error perro");
                    console.log(data);
                    return 0;
                }
            });





}
function deleteCitaFromId(id){

    var ur = "http://" + config.ipAdress + "/server-odonto/DC/" + id;
    $.ajax({
        type: "GET",
        url: ur,
        success: function OnGetMemberSuccess(data) {
            config.mensaje ="Operación realizada satisfactoriamente";
        },
        error: function OnGetMemberSuccess(data, status) {

            console.log(data);

            config.mensaje="Error en el servidor";
        }
    });


}
function getCitasDB(){
    var year = new Date().getFullYear();
    var month = new Date().getMonth();
    var day = new Date().getDate();
    var nuevoMes =  parseInt(month.toString())+1;
    var fechaFinal = year+'-'+nuevoMes+'-'+day;
    var ur = "http://" + config.ipAdress + "/server-odonto/citasControl/" + fechaFinal + "/" + 1;
    $.ajax({
        type: "GET",
        url: ur,
        async:false,
        success: function OnGetMemberSuccess(data) {
            config.listResult=data;
            //console.log(config.listResult);
        },
        error: function OnGetMemberSuccess(data, status) {
            console.log("Errrorrrr");
        }
    });
    var dbVar = {
        dbList:[]
    };
}
function promote(){
    $(document).ready(function() {
        $('#calendar').weekCalendar({
            data: config.listResult
        });

    });
}
function full(){

    $(document).ready(function() {
        config.now2Date = new Date();
        var year = config.now2Date.getFullYear();
        var month = config.now2Date.getMonth();
        var day = config.now2Date.getDate();
        var nuevoMes =  parseInt(month.toString())+1;
        var fechaFinal = year+'-'+nuevoMes+'-'+day;
        config.nowDate = fechaFinal;

        var ur = "http://" + config.ipAdress + "/server-odonto/citasControl/" + fechaFinal + "/" + 1;

        $.ajax({
            type: "GET",
            url: ur,
            async:false,
            success: function OnGetMemberSuccess(data) {
                config.listResult=data;
                $('#calendar').weekCalendar({
                    data: data,
                    minDate:  new Date(),
                    maxDate: new Date(),
                    switchDisplay: {'Hoy': 1,'Semana': 5},
                    timeslotsPerHour: 4,
                    height: function($calendar) {
                        return $(window).height() - $('h1').outerHeight() - $('.description').outerHeight();
                    },
                    eventClick: function(calEvent, $event) {

                    },
                    eventRender: function(calEvent, $event) { },
                    eventDrop: function(calEvent, $event) {},
                    eventResize: function(calEvent, $event) {},
                    eventMouseover: function(calEvent, $event){},
                    eventMouseout: function(calEvent, $event) {},
                    noEvents: function() {},
                    reachedmindate: function($calendar, date) {},
                    reachedmaxdate: function($calendar, date) {}
                });
            },
            error: function OnGetMemberSuccess(data, status) {
                console.log(data);
            }
        });

    });


}
function main_change(){

    if($('#state_event').val() == 'inhabilitada') {
        $('#state_event').css('margin-left', '20%');
        $('#input_main').css('display', 'block');
        $('#input_main').css('margin-left', '50%');   //margin-left: 40px; margin-top: -50px;
        $('#input_main').css('margin-top', '-23px');

    }
    else{
        $('#state_event').css('margin-left', '40%');
        $('#input_main').css('display', 'none');
    }

}
full();
