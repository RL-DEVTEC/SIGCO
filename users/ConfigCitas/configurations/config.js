/**
 * Created by Roberto on 2/3/2018.
 */
function divAlertNoticeWithoutAssign(eventId,tipoD){
    var cambioEstado = 0;

   // alert(eventId);


    alertify.confirm("<h3 style='margin-left: 120px;'>Seleccione operación</h3>",   // select debe estar en 140, cuando onchange 60
        "<div id='select_div' style='margin-left: 60px;'><select id='add_select' onchange='changeObjects();' style='font-size:15px;' >" +
        "<option style='background: #e10000' value='inhabilitar'>Inhabilitar</option>" +
        "<option  value='asignar'>Asignar a estudiante</option>" +
        "</select></div><div id='div_info' style='margin-left: 230px; margin-top: -25px;'>" +
        "<input style='display: none' placeholder='Carné' id='input_est' type='number'><input id='input_Inh' placeholder='Descripción' type='text'>" +
        "<button id='button_search' onclick='cambioCarne()' class='btn btn-info' style='display:none; background-color: #00acc1; margin-left: 170px; margin-top: -28px;'><span class='glyphicon glyphicon-search' aria-hidden='true'></span></button></div>" +   // muestra resultado -53px cuando no -38px;
        "<label id='label_stu' style='margin-left: 230px; '><h5 id='stuFound'></h5></label>"
        ,
        function(){

            if($("#add_select").val()=='asignar' && ($("#input_est").val() == '' || $('#label_stu').text() == '')){ // asigna estudiantes
                alertify.error('Error en asignación de cita, datos incompletos');
            }
            else if($("#add_select").val()=='inhabilitar' && $("#input_Inh").val()==''){
                alertify.error('Error en operación, datos incompletos');
            }
            else{ // el usuario planea inhabilitar   aqui

                // falta agregar la cita

                var fechaFinal = config.returnVal;

                var myPos=0;

                //console.log($('.wc-cal-event'));


                var contador = 1;

                if(tipoD==2){

                    //alert("Next process");
                    $('.wc-cal-event').each(function() {
                       // alert($(this).data('calEvent').id);
                        if($(this).data('calEvent').id==eventId){

                            myPos+=1;
                            //alert("Search");
                            return false;

                        }
                        else{
                            myPos+=1;
                        }

                    });

                   // alert(myPos);
                    //myPos+=1;

                }
                else{
                    $('.wc-cal-event').each(function() {
                       // alert($(this).data('calEvent').id);
                        if($(this).data('calEvent').id<0){
                            myPos+=1;
                            return false;

                        }
                        if($(this).data('calEvent').id>0){
                            myPos+=1;

                        }
                        else{
                            return false;
                        }


                    });

                    //alert(myPos);
                }



                var find =0;
                var conG = 0;

                    $('.wc-time').each(function() { // buscar el evento según la posición de el array web
                        conG += 1;
                        //console.log("Viene");
                        //console.log("pre");
                        //alert(contador);
                        if(contador==myPos){

                            var toRecord = $(this).text();

                            var horaI = "";
                            var horaF = "";

                            for(var i = 0; i<5; i++){
                                horaI += toRecord[i];
                            }
                            for(var i = 11; i<16; i++){
                                horaF += toRecord[i];
                            }

                           // alert(horaI+" "+horaF);
                            if(tipoD==1){ // 1 -> inserción normal  // 2 -> inserción con pre eliminación de cita inhabilitada

                                //alert(horaI + " hasta "+ horaF);
                                if($("#add_select").val()=='inhabilitar'){

                                    creaNuevaCita("no", fechaFinal, horaI, horaF,3,0,$("#input_Inh").val().toString());
                                }
                                else{

                                    creaNuevaCita($("#input_est").val(), fechaFinal, horaI, horaF,1,0,"sin");
                                }
                                getIDcitaInsertada(horaI,horaF,fechaFinal);
                                var cou = 0;
                                $('.wc-cal-event').each(function() {
                                    if($(this).data('calEvent').id <0){

                                        if(config.consult==0){
                                            alertify.error('Error en el servidor');
                                        }
                                        else{
                                            // alert("Id asignado: "+config.consult.toString());
                                            $(this).data('calEvent').id = config.consult;
                                            //alert("Asignado"+$(this).data('calEvent').id.toString());
                                            //alert($(this).data('calEvent').id);
                                            if($("#add_select").val()=='inhabilitar'){
                                                $(this).css('backgroundColor', '#FF0000');



                                            }
                                            updateTitleFromDate(config.consult,cou);

                                            alertify.success('¡ Inserción exitosa !');
                                        }

                                    }
                                    else{
                                        cou+=1;
                                    }});
                                return false;
                                // },400);

                            }
                            else{

                               // alert("Do it");
                                console.log("do it");
                                console.log($("#input_est").val());
                                console.log(fechaFinal);
                                alert(horaI +" hasta" +horaF);
                                console.log(eventId);

                                creaNuevaCita($("#input_est").val(), fechaFinal, horaI, horaF,2,eventId,"sin");



                                getIDcitaInsertada(horaI,horaF,fechaFinal);

                                var cou = 0;

                                $('.wc-cal-event').each(function() {
                                    if($(this).data('calEvent').id == eventId){

                                        if(config.returnVal==0){
                                            alertify.error('Error en el servidor');
                                        }
                                        else{
                                            $(this).data('calEvent').id = config.consult;
                                            return false;
                                        }

                                    }
                                    else{
                                        cou+=1;
                                    }});
                                return false;


                            }

                        }
                        else{
                            contador+=1;
                        }

                });
                //alert("Cong: "+ conG.toString());
            }


        },
        function(){
            alertify.error('Cancel');

        });
    $("#input_est").val('');
    $('#label_stu').text('');
}
function posAfterLoad(){
    $('.wc-cal-event').each(function() {
        var eventId2 = $(this).data('calEvent').id ;
        var listCitas = config.listResult;

        //alert("Lo hace");

        for(var i = 0; i<listCitas.length ; i++){

            if(listCitas[i].id === eventId2){
                var estado = listCitas[i].state;

                if(estado=="pendiente"){
                    //.css('backgroundColor', '#31B404'); $(this).css('backgroundColor', '#31B404');
                    $(this).css('backgroundColor', '#68a1e5');
                }
                else if(estado=="noshow"){
                    $(this).css('backgroundColor', '#F7FE2E');
                }
                else if(estado=="realizada"){
                    $(this).css('backgroundColor', '#31B404');
                }
                else{// inhabilidada
                    $(this).css('backgroundColor', '#FF0000');
                }
            }
        }
    });
}
function eliminarEvento(eventId){
    $('.wc-cal-event').each(function() {
        if ($(this).data('calEvent').id == eventId) {
            $(this).remove();
            return false;
        }
    });
}
function getNowDate(){
    var year = new Date().getFullYear();
    var month = new Date().getMonth();
    var day = new Date().getDate();
    var nuevoMes =  parseInt(month.toString())+1;
    var fechaFinal = year+'-'+nuevoMes+'-'+day;

    return fechaFinal;
}
function divAlertsNoticeWithAssign(eventId) {

    if(!alertify.Confirm){
        //define a new errorAlert base on alert
        alertify.dialog('Confirm',function factory(){
            return{
                build:function(){
                    var errorHeader = '<span class="fa fa-times-circle fa-2x" '
                        +    'style="align:center;color:#e10000;">'
                        + '</span> Seleccione el estado';
                    this.setHeader(errorHeader);
                }
            };
        },true,'alert');
    }


    var cambioEstado = 0;

    alertify.confirm("<h3 style='margin-left: 130px;'>Seleccione el estado</h3>",
        "<select id='state_event' style='margin-left: 40%;' onchange='main_change();' style='margin-left: 160px; font-size:15px;' >" +
        "<option style='background: #68a1e5' value='pendiente'>Pendiente</option>" +
        "<option style='background: #F7FE2E' value='noshow'>No se presentó</option>" +
        "<option style='background: #31B404' value='realizada'>Realizada</option>" +
        "<option style='background: #FF0000' value='inhabilitada'>Inhabilitar</option>" +
        "</select><input id='input_main' style='display: none; ' type='text' placeholder='Indique una descripción'></input>",
        function(){

            var seleccion = $("#state_event").val();

            var descrip = $("#input_main").val();

            if(descrip == ""){
                descrip = "Sin descripción";
            }
            // consultarDescCita(eventId);
            cambiarEstadoCita(eventId,seleccion,descrip);



            if(config.mensaje != "Error en el servidor"){


                alertify.success('Operación satisfactoria');


                setTimeout(function(){

                    var cont=0; //$('.wc-time').each(function() {
                    $('.wc-cal-event').each(function() {
                        if ($(this).data('calEvent').id === eventId) {


                            updateTitleFromDate(eventId,cont);


                            if(seleccion=="pendiente"){
                                $(this).css('backgroundColor', '#68a1e5');
                                // $(".wc-time.ui-corner-top").css('backgroundColor', '#68a1e5');

                            }
                            else if(seleccion=="noshow"){
                                $(this).css('backgroundColor', '#F7FE2E');
                                //   $(".wc-time.ui-corner-top").css('backgroundColor', '#F7FE2E');
                            }
                            else if(seleccion=="realizada"){
                                $(this).css('backgroundColor', '#31B404');
                                //  $(".wc-time.ui-corner-top").css('backgroundColor', '#31B404');
                            }
                            else{
                                $(this).css('backgroundColor', '#FF0000');
                                //$(".wc-time.ui-corner-top").css('backgroundColor', '#FF0000');
                            }




                            return false;
                        }
                        cont+=1;
                    });
                }, 500);


            }
            else{
                alertify.error(config.mensaje);
            }


        },
        function(){
            alertify.error('Cancel');
        });

}

function updateTitleFromDate(idCita,cont){

   consultarEstado(idCita);
        if(config.consult == 1){ // asociado a inhab
            consultarDescCita(idCita);
        }
        else if(config.consult == 2){// asociado a estudiante
            consultEstAsignado(idCita);
        }

            if(config.consult != 0 && config.consult!=3){
                mainTitle = 0
                var subCont=0;
                $('.wc-title').each(function() {
                    if(subCont==cont){
                        if(mainTitle==0){
                            mainTitle+=1
                        }
                        else{
                            $(this).text(config.returnVal);
                            return false;
                        }

                    }
                    else{
                        subCont+=1;
                    }
                });
            }



            config.consult=0;





}