angular.module('userModule')
    .controller("enfermedadCtrl", function($scope,$location,$route,$http,$window) {
        $scope.listaEnfermedades = [];
        $scope.getEnfermedades = function getEnfermedades() {
            var ur="http://"+config.ipAdress+"/server-odonto/enfermedades";
                $http({
                    method: "GET",
                    url: ur
                }).then(function mySucces(response) {

                    var data=response.data;
                    $scope.listaEnfermedades=response.data;
                });
        };
        $scope.reloadRoute = function() {
            $route.reload();
        };

        $scope.guardarEnfermedad =  function guardarEnfermedad() {
            var enfermedad = $("#enfermedad").val();
            var descripcion = $("#descripcion").val();
            var tratamiento = $("#tratamiento").val();
            if(enfermedad === "" || descripcion === "" || tratamiento === ""){
                alertify.error("Complete los datos!");

            }else{
                var datos ={ 'Enfermedad' : enfermedad ,'descripcion' :  descripcion , 'tratamiento' : tratamiento  };
                var ur = "http://"+config.ipAdress+"/server-odonto/Enfermedades/Enfermedad";
                $http({
                    method: "POST",
                    url: ur,
                    data: datos
                }).success(function (response) {
                    alertify.success("Enfermedad agregada!");
                    $scope.listaEnfermedades.push(datos);
                    $("#enfermedad").val("");
                    $("#descripcion").val("");
                    $("#tratamiento").val("");
                }).error(function(err){
                    alertify.error("Hubo un error");
                });
            }
        };

        $scope.editEnfermedades =  function editEnfermedades(enfermedad) {

            var id = $("#edit-form-id").val();
            var name = $("#edit-form-enfermedad").val();
            var des = $("#edit-form-descripcion").val();
            var tra = $("#edit-form-tratamiento").val();

            if(id === "" || name=== "" ||des ==="" || tra===""){

                alertify.success("Complete los datos!");

            }
            else{
                var datos = "{ 'idEnfermedad' : '"+id+"','enfermedad' :'" + name + "', 'descripcion' : '" + des + "', 'tratamiento' : '" + tra + "' }";
                console.log(datos);
                var ur = "http://"+config.ipAdress+"/server-odonto/Enfermedades2/Enfermedad2";
                $http({
                    method: "POST",
                    url: ur,
                    data: datos
                }).success(function (result) {
                    alertify.success("Enfermedad editada.");
                    //$scope.listaEnfermedades.splice(id, 1);
                    //$scope.listaEnfermedades.push(datos);
                    $window.location.reload();
                }).error(function(err){
                    alertify.error("Hubo un error");
                })
            }
        };


        $scope.deleteEnfermedad = function (enfermedad){

            var index = $scope.listaEnfermedades.indexOf(enfermedad);
            if (index > -1) {
                $scope.listaEnfermedades.splice(index, 1);
            }
            var id=enfermedad.idEnfermedad;

            /*var id = variable;*/
            var url = "http://"+config.ipAdress+"/server-odonto/Enfermedades/Enfermedad/"+id;
            console.log(url);
            if (confirm('Seguro de borrar la enfermedad?')) {
                $http({
                    method: 'GET',
                    url: url,

                    success: function (result) {

                    },
                    error: function (err) {
                        console.log(err);
                    }

                });
            }/**/
        };

        $scope.editarEnfermedad = function editarEnfermedad(enfermedad) {

            $('#edit-form-id').val(enfermedad.idEnfermedad);

            $('#edit-form-enfermedad').val(enfermedad.Enfermedad);//($(this).data('enfermedad'));

            $('#edit-form-descripcion').val(enfermedad.descripcion);

            $('#edit-form-tratamiento').val(enfermedad.tratamiento);

        };

        $scope.getEnfermedades();

    });