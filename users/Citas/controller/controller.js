angular.module('userModule')
    .controller('dashboardCitasCtrl', function($scope,MessageResource) {
        /* config object */
        var year = new Date().getFullYear();
        var month = new Date().getMonth();
        var day = new Date().getDate();
        $(document).ready(function() {
            var year = new Date().getFullYear();
            var month = new Date().getMonth();
            var day = new Date().getDate();
            var nuevoMes =  parseInt(month.toString())+1;
            var fechaFinal = year+'-'+nuevoMes+'-'+day;
            var ur = "http://" + config.ipAdress + "/server-odonto/citasControl/" + fechaFinal + "/" + 1;
            $.ajax({
                type: "GET",
                url: ur,
                success: function OnGetMemberSuccess(datas) {
                    config.listResult=datas;
                    $('#calendar').weekCalendar({
                        data: datas,
                        minDate:  new Date(),
                        maxDate: new Date(),
                        switchDisplay: {'Hoy': 1,'Semana': 5},
                        timeslotsPerHour: 4,
                        height: function($calendar) {
                            return $(window).height() - $('h1').outerHeight() - $('.description').outerHeight();
                        },
                        eventRender: function(calEvent, $event) {},
                        eventDrop: function(calEvent, $event) {},
                        eventResize: function(calEvent, $event) {},
                        eventClick: function(calEvent, $event) {

                            alertify.confirm('Información de la Cita', "" +
                                "<div>Nombre:</div>"+calEvent.title+
                                "<div>Carné:</div>"+"dd"+
                                "<div>Carrera:</div>"+"jj", function(){ alertify.success('Ok') }
                                , function(){ alertify.error('Cancel')});




                        },
                        eventMouseover: function(calEvent, $event) {},
                        eventMouseout: function(calEvent, $event) {},
                        noEvents: function() {},
                        reachedmindate: function($calendar, date) {
                            alert('You reached mindate');
                        },
                        reachedmaxdate: function($calendar, date) {}
                    });
                },
                error: function OnGetMemberSuccess(data, status) {
                    console.log(data);
                }
            });
        });
    });