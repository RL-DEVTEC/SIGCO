angular.module('userModule')
    .controller("administradorCtrl", function($scope,$http) {
        $scope.administradores = [];
        $scope.getAdministrador = function getAdministrador() {

            var ur="http://"+config.ipAdress+"/server-odonto/Administradores";
            $http({
                method: "GET",
                url: ur
            }).then(function mySucces(response) {

                var data=response.data;
                $scope.administradores=response.data;
            });
        };

        $scope.setAdministrador  =  function setAdministrador() {
            var nombre = $("#nombre").val();
            var cedula = $("#cedula").val();
            var direccion = $("#direccion").val();
            var telefono = $("#telefono").val();
            var email = $("#email").val();
            var tipo = $("#tipo").val();


            var datos = "{ 'nombre' :'" + nombre +"', 'cedula' :'" + cedula + "', 'direccion' : '" + direccion + "', 'telefono' : '" + telefono + "', 'email' :'" + email+"', 'tipo' : '" + tipo + "' }";
            console.log(datos);
            var ur = "http://"+config.ipAdress+"/server-odonto/Enfermedades/Enfermedad";
            console.log(ur);
            if (confirm('Está seguro de insertar la Enfermedad?')) {
                $http({
                    method: "POST",
                    url: ur,
                    data: datos
                }).then(function mySucces(response) {
                    console.log("dcndskncncsk")
                });
            }

        };

        $scope.editAdministrador =  function editAdministrador(enfermedad) {

            var nombre = $("#edit-form-nombre").val();
            var cedula = $("#edit-form-cedula").val();
            var direccion = $("#edit-form-direccion").val();
            var telefono = $("#edit-form-telefono").val();
            var email = $("#edit-form-email").val();
            var tipo = $("#edit-form-tipo").val();

            var datos = "{ 'nombre' :'" + nombre +"', 'cedula' :'" + cedula + "', 'direccion' : '" + direccion + "', 'telefono' : '" + telefono + "', 'email' :'" + email+"', 'tipo' : '" + tipo + "' }";
            console.log(datos);
            var ur = "http://"+config.ipAdress+"/server-odonto/Enfermedades2/Enfermedad2";
            console.log(ur);
            if (confirm('Está seguro de editar la información?')) {
                $http({
                    method: "POST",
                    url: ur,
                    data: datos
                }).success(function (result) {


                    alert("si ");

                }).error(function(err){
                    alert("No ");
                })
            }

        };


        $scope.deleteAdministrador = function deleteAdministrador(variable){
            var id = variable;
            var url = "http://"+config.ipAdress+"/server-odonto/Enfermedades/Enfermedad/"+id;
            console.log(url);
            if (confirm('Seguro de borrarlo?')) {
                $http({
                    method: 'GET',
                    url: url,

                    success: function (result) {
                        console.log('Deleting...');

                    },
                    error: function (err) {
                        console.log(err);
                    }

                });
            }
        };

        $scope.editarAdministrador = function editarAdministrador(administrador) {

            $('#edit-form-nombre').val(administrador.nombre);

            $('#edit-form-cedula').val(administrador.cedula);//($(this).data('enfermedad'));

            $('#edit-form-direccion').val(administrador.direccion);

            $('#edit-form-telefono').val(administrador.telefono);

            $('#edit-form-email').val(administrador.email);

            $('#edit-form-tipo').val(administrador.tipo);
        };

        $scope.getAdministrador();

    });