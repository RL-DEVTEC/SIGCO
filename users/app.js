angular.module('userModule',["ngRoute","ngResource"])
    .config(['$routeProvider',function($routeProvider)
    {
        $routeProvider.when("/user",{
            templateUrl:'Citas/citas.html',
            controller: 'dashboardCitasCtrl'
        })
            .when("/user/Enfermedad",{
                templateUrl:'Enfermedades/index.html',
                controller: 'enfermedadCtrl'
            })
            .when("/user/Administrador",{
                templateUrl:'Administradores/index.html',
                controller: 'enfermedadCtrl'
            })
            .when("/user/ConfigCitas",{
                templateUrl:'ConfigCitas/configCitas.html',
                controller: 'ConfigCitasCtrl'
            })
            .when("/user/Estudiantes",{
                templateUrl:'Estudiantes/Estudiantes.html',
                controller: 'estudiantesCtrl'
            })
            .when("/user/Examen",{
                templateUrl:'ExamenClinico/index.html',
                controller: 'examenCtrl'
            })
            .when("/user/Estadisticas",{
                templateUrl:'Estadisticas/estadisticas.html',
                controller: 'estadisticasCtrl'
            })
    }
    ]);