/**
 * Created by Roberto on 16/2/2018.
 */
angular.module('userModule')
    .controller('estudiantesCtrl', function($scope,$http) {
        $scope.currentPage = 0;
        $scope.pageSize = 5;
        //___________________________________________________________________________________________________________
        //___________________________________________________________________________________________________________
        $scope.configPages = function() {
            $scope.pages.length = 0;
            var ini = $scope.currentPage - 2;
            var fin = $scope.currentPage + 3;
            if (ini < 1) {
                ini = 1;
                if (Math.ceil($scope.listaEstudiantes.length / $scope.pageSize) > 10000)
                    fin = 10000;
                else
                    fin = Math.ceil($scope.listaEstudiantes.length / $scope.pageSize);
            } else {
                if (ini >= Math.ceil($scope.listaEstudiantes.length / $scope.pageSize) - 10000) {
                    ini = Math.ceil($scope.listaEstudiantes.length / $scope.pageSize) - 10000;
                    fin = Math.ceil($scope.listaEstudiantes.length / $scope.pageSize);
                }
            }
            if (ini < 1) ini = 1;
            for (var i = ini; i <= fin; i++) {
                $scope.pages.push({
                    no: i
                });
            }

            if ($scope.currentPage >= $scope.pages.length)
                $scope.currentPage = $scope.pages.length - 1;
        };
        $scope.setPage = function(index) {
            $scope.currentPage = index - 1;
        };
        //____________________________________________________________________________________________________
        $scope.pages = [];
        $scope.buscador=[{carne:"12",pin:"45",cedula:"126", nombre:"Fernando",ape1:"muñoz",ape2:"Castro",
            carneCCSS:"45", telefono:"154", carrera:"Administracion", becado:"MC",sexo:"Masculino", residente:"No", edad:"18",
            estadoCivil: "casado", nacimiento: "2005-12-31"},{carne:"4",pin:"83",cedula:"226", nombre:"Royland",ape1:"Solis",
            ape2:"Chacon", carneCCSS:"875", telefono:"4821", carrera:"CA", becado:"MC",sexo:"M",
            residente:"No", edad:"18", estadoCivil: "soltero", nacimiento: "15/01/2011"},{carne:"5",pin:"83",cedula:"226", nombre:"Diego",ape1:"Solis",
            ape2:"Chacon", carneCCSS:"875", telefono:"4821", carrera:"CA", becado:"MC",sexo:"M",
            residente:"No", edad:"18", estadoCivil: "soltero", nacimiento: "45"},{carne:"6",pin:"83",cedula:"226", nombre:"Leidy",ape1:"Solis",
            ape2:"Chacon", carneCCSS:"875", telefono:"4821", carrera:"CA", becado:"MC",sexo:"M",
            residente:"No", edad:"18", estadoCivil: "soltero", nacimiento: "45"},{carne:"8",pin:"83",cedula:"227", nombre:"Roberto",ape1:"Solis",
            ape2:"Chacon", carneCCSS:"875", telefono:"4821", carrera:"CA", becado:"MC",sexo:"M",
            residente:"No", edad:"18", estadoCivil: "soltero", nacimiento: "45"},{carne:"9",pin:"83",cedula:"228", nombre:"Roberto",ape1:"Solis",
            ape2:"Chacon", carneCCSS:"875", telefono:"4821", carrera:"CA", becado:"MC",sexo:"M",
            residente:"No", edad:"18", estadoCivil: "soltero", nacimiento: "45"},{carne:"10",pin:"83",cedula:"229", nombre:"Roberto",ape1:"Solis",
            ape2:"Chacon", carneCCSS:"875", telefono:"4821", carrera:"CA", becado:"MC",sexo:"M",
            residente:"No", edad:"18", estadoCivil: "soltero", nacimiento: "45"},{carne:"11",pin:"83",cedula:"230", nombre:"Roberto",ape1:"Solis",
            ape2:"Chacon", carneCCSS:"875", telefono:"4821", carrera:"CA", becado:"MC",sexo:"M",
            residente:"No", edad:"18", estadoCivil: "soltero", nacimiento: "45"},{carne:"12",pin:"83",cedula:"240", nombre:"Roberto",ape1:"Solis",
            ape2:"Chacon", carneCCSS:"875", telefono:"4821", carrera:"CA", becado:"MC",sexo:"M",
            residente:"No", edad:"18", estadoCivil: "soltero", nacimiento: "45"},{carne:"13",pin:"83",cedula:"241", nombre:"Roberto",ape1:"Solis",
            ape2:"Chacon", carneCCSS:"875", telefono:"4821", carrera:"CA", becado:"MC",sexo:"F",
            residente:"No", edad:"18", estadoCivil: "soltero", nacimiento: "45"},{carne:"13",pin:"83",cedula:"242", nombre:"Roberto",ape1:"Solis",
            ape2:"Chacon", carneCCSS:"875", telefono:"4821", carrera:"CA", becado:"MC",sexo:"F",
            residente:"No", edad:"18", estadoCivil: "soltero", nacimiento: "45"},{carne:"14",pin:"83",cedula:"243", nombre:"Roberto",ape1:"Solis",
            ape2:"Chacon", carneCCSS:"875", telefono:"4821", carrera:"CA", becado:"MC",sexo:"F",
            residente:"No", edad:"18", estadoCivil: "soltero", nacimiento: "45"}];
        //$scope.listaEstudiantes = [];
        $scope.listaEstudiantes  = [{carne:"12",pin:"45",cedula:"126", nombre:"Fernando",ape1:"muñoz",ape2:"Castro",
            carneCCSS:"45", telefono:"154", carrera:"Administracion", becado:"MC",sexo:"Masculino", residente:"No", edad:"18",
            estadoCivil: "casado", nacimiento: "2005-12-31"},{carne:"4",pin:"83",cedula:"226", nombre:"Royland",ape1:"Solis",
            ape2:"Chacon", carneCCSS:"875", telefono:"4821", carrera:"CA", becado:"MC",sexo:"M",
            residente:"No", edad:"18", estadoCivil: "soltero", nacimiento: "15/01/2011"},{carne:"5",pin:"83",cedula:"226", nombre:"Diego",ape1:"Solis",
            ape2:"Chacon", carneCCSS:"875", telefono:"4821", carrera:"CA", becado:"MC",sexo:"M",
            residente:"No", edad:"18", estadoCivil: "soltero", nacimiento: "45"},{carne:"6",pin:"83",cedula:"226", nombre:"Leidy",ape1:"Solis",
            ape2:"Chacon", carneCCSS:"875", telefono:"4821", carrera:"CA", becado:"MC",sexo:"M",
            residente:"No", edad:"18", estadoCivil: "soltero", nacimiento: "45"},{carne:"8",pin:"83",cedula:"226", nombre:"Roberto",ape1:"Solis",
            ape2:"Chacon", carneCCSS:"875", telefono:"4821", carrera:"CA", becado:"MC",sexo:"M",
            residente:"No", edad:"18", estadoCivil: "soltero", nacimiento: "45"},{carne:"9",pin:"83",cedula:"226", nombre:"Roberto",ape1:"Solis",
            ape2:"Chacon", carneCCSS:"875", telefono:"4821", carrera:"CA", becado:"MC",sexo:"M",
            residente:"No", edad:"18", estadoCivil: "soltero", nacimiento: "45"},{carne:"10",pin:"83",cedula:"226", nombre:"Roberto",ape1:"Solis",
            ape2:"Chacon", carneCCSS:"875", telefono:"4821", carrera:"CA", becado:"MC",sexo:"M",
            residente:"No", edad:"18", estadoCivil: "soltero", nacimiento: "45"},{carne:"11",pin:"83",cedula:"226", nombre:"Roberto",ape1:"Solis",
            ape2:"Chacon", carneCCSS:"875", telefono:"4821", carrera:"CA", becado:"MC",sexo:"M",
            residente:"No", edad:"18", estadoCivil: "soltero", nacimiento: "45"},{carne:"12",pin:"83",cedula:"226", nombre:"Roberto",ape1:"Solis",
            ape2:"Chacon", carneCCSS:"875", telefono:"4821", carrera:"CA", becado:"MC",sexo:"M",
            residente:"No", edad:"18", estadoCivil: "soltero", nacimiento: "45"},{carne:"13",pin:"83",cedula:"226", nombre:"Roberto",ape1:"Solis",
            ape2:"Chacon", carneCCSS:"875", telefono:"4821", carrera:"CA", becado:"MC",sexo:"F",
            residente:"No", edad:"18", estadoCivil: "soltero", nacimiento: "45"},{carne:"13",pin:"83",cedula:"226", nombre:"Roberto",ape1:"Solis",
            ape2:"Chacon", carneCCSS:"875", telefono:"4821", carrera:"CA", becado:"MC",sexo:"F",
            residente:"No", edad:"18", estadoCivil: "soltero", nacimiento: "45"},{carne:"14",pin:"83",cedula:"226", nombre:"Roberto",ape1:"Solis",
            ape2:"Chacon", carneCCSS:"875", telefono:"4821", carrera:"CA", becado:"MC",sexo:"F",
            residente:"No", edad:"18", estadoCivil: "soltero", nacimiento: "45"}];
        $scope.getEstudiante = function getEstudiante() {
            var ur="http://"+config.ipAdress+"/server-odonto/estudiantes";
            $http({
                method: "GET",
                url: ur
            }).then(function mySucces(response) {
                $scope.changeSpaces(response.data);
            });
        };

        $scope.changeSpaces = function changeSpaces(listEst){
            //$scope.listaEstudiantes  = [{carne:"12",pin:"45",cedula:"126", nombre:"fernando",ape1:"muñoz",ape2:"Castro",
            //  carneCCSS:"45", telefono:"154", carrera:"CA", becado:"MC",sexo:"Q", residente:"No", edad:"18",
            //  estadoCivil: "casado", nacimiento: "15"},{carne:"4",pin:"83",cedula:"226", nombre:"Royland",ape1:"Solis",
            //   ape2:"Chacon", carneCCSS:"875", telefono:"4821", carrera:"CA", becado:"MC",sexo:"M",
            //    residente:"No", edad:"18", estadoCivil: "soltero", nacimiento: "45"}];
            $scope.listaEstudiantes = [];
            $scope.buscador=[];
            for(var i =0; i< listEst.length;i++){
                if(listEst[i].becado==1){
                    listEst[i].becado="MC";
                }
                else if(listEst[i].becado===2){
                    listEst[i].becado="Prest";
                }
                else
                    listEst[i].becado="No";

                if(listEst[i].residente=="1"){
                    listEst[i].residente="Si";
                }
                else
                    listEst[i].residente="No";
                var miJson = {carne:listEst[i].carne,pin:listEst[i].pin.toString(),cedula:listEst[i].cedula,
                    nombre:listEst[i].nombre,ape1:listEst[i].ape1,ape2:listEst[i].ape2, carneCCSS:listEst[i].carneCCSS,
                    telefono:listEst[i].telefono.toString(), carrera:listEst[i].carrera, becado:listEst[i].becado,sexo:listEst[i].sexo,
                    residente:listEst[i].residente, edad:listEst[i].edad.toString(), estadoCivil: listEst[i].estadoCivil,
                    nacimiento: listEst[i].nacimiento};
                $scope.buscador.push(miJson);
                $scope.listaEstudiantes.push(miJson);

            }
        };
        $scope.buscarEstudiante=function buscarEstudiante() {
            $scope.listaEstudiantes=[];
            var input, filter, table, varCarnet, varNombre, i, varCarrera, varCedula, varBeca, varSexo, varEdad,
                varTabla, varApellido1, varApellido2;
            input = document.getElementById("StyleBarraBusqueda");
            filter = input.value.toUpperCase();
            table = document.getElementById("myTable");
            varTabla = table.getElementsByTagName("tr");
            if(filter===""){
                $scope.listaEstudiantes=$scope.buscador;
            }
            else {
                for (i = 0; i < $scope.buscador.length; i++) {
                    if ($scope.buscador[i].carne.toUpperCase().indexOf(filter) > -1 || $scope.buscador[i].nombre.toUpperCase().indexOf(filter) > -1
                        || $scope.buscador[i].ape1.toUpperCase().indexOf(filter) > -1 || $scope.buscador[i].ape2.toUpperCase().indexOf(filter) > -1
                        || $scope.buscador[i].carrera.toUpperCase().indexOf(filter) > -1 || $scope.buscador[i].cedula.toUpperCase().indexOf(filter) > -1
                        || $scope.buscador[i].becado.toUpperCase().indexOf(filter) > -1 || $scope.buscador[i].sexo.toUpperCase().indexOf(filter) > -1
                        || $scope.buscador[i].edad.toUpperCase().indexOf(filter) > -1) {

                        $scope.listaEstudiantes.push($scope.buscador[i])
                    }
                }
            }
        };


        //EJEMPLO
        $scope.searchcedula = function searchcedula() {
            var ur= "http://"+config.ipAdress+"/server-odonto/estudiantes";
            if($('#cedulaEstudiante').val()===""){
                console.log('Vacio');
            }

            else{
                $http.get(ur+$('#cedulaEstudiante').val())
                .then(function (response) {$scope.listilla=response.data});
                $scope.listilla={carne:"12",pin:"45",cedula:"126", nombre:"Fernando",ape1:"muñoz",ape2:"Castro",
                    carneCCSS:"45", telefono:"8818-9624", carrera:"Computacion", becado:"Prest",sexo:"Masculino", residente:"No", edad:"18",
                    estadoCivil: "casado", nacimiento: "2005-12-31"};
                if($scope.listilla===null){
                    dialogs('mostrar','exito','No existe estudiante');
                }
                else{// lo que hace es recibir los datos del estudiante
                    console.log('pssssssssssss');
                    $('#nombreEstudiante').val($scope.listilla.nombre);
                    $('#carneEstudiante').val($scope.listilla.carne);
                    $('#apellidoUnoEstudiante').val($scope.listilla.ape1);
                    $('#apellidoDosEstudiante').val($scope.listilla.ape2);
                    $('#carreraEstudiante').val($scope.listilla.carrera);
                    $('#becaEstudiante').val($scope.listilla.becado);
                    $('#sexoEstudiante').val($scope.listilla.sexo);
                    $('#edadEstudiante').val($scope.listilla.edad);
                    $('#carnetCajaEstudiante').val($scope.listilla.carneCCSS);
                    $('#telefonoEstudiante').val($scope.listilla.telefono);
                    $('#residenteEstudiante').val($scope.listilla.residente);
                    $('#nacimientoEstudiante').val($scope.listilla.nacimiento);
                    dialogs('mostrar','exito','Información cargada exitosamente');
                }
            }
        };


        $scope.setEstudiante =  function setEstudiante() {
            var nombreEstudiante = $("#nombre").val();
            var apellidoUnoEstudiante = $("#apellido1").val();
            var apellidoDosEstudiante = $("#apellido2").val();
            var carneEstudiante = $("#carnet").val();
            var carreraEstudiante = $("#carrera").val();
            var cedulaEstudiante = $("#cedula").val();
            var becaEstudiante = $("#beca").val();
            var sexoEstudiante = $("#sexo").val();
            var edadEstudiante = $("#edad").val();
            var carneCajaEstudiante= $("#carneCaja").val();
            var telefonoEstudiante= $("#telefono").val();
            var residenteEstudiante=$("#residente").val();
            var nacimientoEstudiante=$("#nacimiento").val();
            alertify.success("add");
            var datos ={ 'nombre' : +nombreEstudiante ,'carne' :  +carneEstudiante ,'apellidoUno': + apellidoUnoEstudiante,
                'apellidoDos': +apellidoDosEstudiante ,'carrera' : + carreraEstudiante, 'cedula' : + cedulaEstudiante,
                'becado': + becaEstudiante, 'sexo': + sexoEstudiante, 'edad': + edadEstudiante, 'carneCaja':+ carneCajaEstudiante,
                'telefono': +telefonoEstudiante,'residente':+residenteEstudiante,'nacimiento':+nacimientoEstudiante};

            $scope.estudiante.push(datos);

            console.log(datos);
            var ur = "http://"+config.ipAdress+"/server-odonto/Estudiantes/estudiante";
            console.log(ur);
            if (confirm('Está seguro de insertar el estudiante?')) {
                $http({
                    method: "POST",
                    url: ur,
                    data: datos
                }).then(function mySucces(response) {
                    console.log("dcndskncncsk")
                });
            }
        };

        $scope.editEstudiante =  function editEstudiante(estudiante) {
            var nombreEstudiantado = $("#edit-form-nombreEstudiante").val();
            var carneEstudiantado = $("#edit-form-carneEstudiante").val();
            var apellidoUnoEstudiantado = $("#edit-form-apellidoUnoEstudiante").val();
            var apellidoDosEstudiantado = $("#edit-form-apellidoDosEstudiante").val();
            var carreraEstudiantado = $("#edit-form-carreraEstudiante").val();
            var cedulaEstudiantado = $("#edit-form-cedulaEstudiante").val();
            var becaEstudiantado = $("#edit-form-becaEstudiante").val();
            var sexoEstudiantado= $("#edit-form-sexoEstudiante").val();
            var edadEstudiantado= $("#edit-form-edadEstudiante").val();
            var carneCajaEstudiantado=$("#edit-form-carneCajaEstudiante").val();
            var telefonoEstudiantado= $("#edit-form-telefonoEstudiante").val();
            var residenteEstudiantado= $("#edit-form-residenteEstudiante").val();
            var nacimientoEstudiantado= $("#edit-form-nacimientoEstudiante").val();

            var datos = "{ 'nombre' : '"+nombreEstudiantado+"','Carnet' :'" + carneEstudiantado + "','Apellido Uno' :'"+ apellidoUnoEstudiantado+ "'," +
                ",'Apellido Dos' :'"+ apellidoDosEstudiantado+ "', 'Carrera' : '" + carreraEstudiantado + "'" +
                ",'Cedula' : '"+cedulaEstudiantado+", 'Beca' : '"+ becaEstudiantado+"','Sexo' :'" + sexoEstudiantado +
                ",'Nacimiento' :'"+nacimientoEstudiantado+",'Residente' :'"+residenteEstudiantado+",'CarneCaja' :'"+carneCajaEstudiantado+",'CarneCaja' :'"+telefonoEstudiantado+",','Edad' :'" + edadEstudiantado+"'," + "'}";
            console.log(datos);
            var ur = "http://"+config.ipAdress+"/server-odonto/Estudiantes2/estudiante2";
            console.log(ur);
            if (confirm('Está seguro de editar el Estudiante?')) {
                $http({
                    method: "POST",
                    url: ur,
                    data: datos
                }).success(function (result) {
                    alert("SI")

                }).error(function(err){
                    alert("No ");
                })
            }
        };
        $scope.deleteEstudiante = function (estudiante){

            var index = $scope.listaEstudiantes.indexOf(estudiante);
            if (index > -1) {
                $scope.estudiante.splice(index, 1);
            }
            var carne=estudiante.carne;
            var url = "http://"+config.ipAdress+"/server-odonto/Estudiantes/Estudiantes/"+carne;
            console.log(url);
            if (confirm('Seguro de borrar el estudiante?')) {
                $http({
                    method: "POST",
                    url: ur,
                    data: datos
                }).success(function (result) {
                    $window.location.reload();
                }).error(function(err){
                    alert("No ");
                })
            }
        };
        $scope.editarEstudiante = function editarEstudiante(estudiante) {

            $('#edit-form-nombreEstudiante').val(estudiante.nombre);

            $('#edit-form-carneEstudiante').val(estudiante.carne);

            $('#edit-form-apellidoUnoEstudiante').val(estudiante.ape1);

            $('#edit-form-apellidoDosEstudiante').val(estudiante.ape2);

            $('#edit-form-carreraEstudiante').val(estudiante.carrera);

            $('#edit-form-cedulaEstudiante').val(estudiante.cedula);

            $('#edit-form-becaEstudiante').val(estudiante.becado);

            $('#edit-form-sexoEstudiante').val(estudiante.sexo);

            $('#edit-form-edadEstudiante').val(estudiante.edad);

            $('#edit-form-carneCajaEstudiante').val(estudiante.carneCCSS);

            $('#edit-form-telefonoEstudiante').val(estudiante.telefono);

            $('#edit-form-residenteEstudiante').val(estudiante.residente);

            $('#edit-form-nacimientoEstudiante').val(estudiante.nacimiento);
        };
        $scope.getEstudiante()
    })
    .filter('startFromGrid', function() {
        return function(input, start) {
            start = +start;
            return input.slice(start);
        }
    });

