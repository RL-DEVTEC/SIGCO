﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace server_odontologia
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");


            routes.MapRoute(
                "AccesoSwitch",
                "SwitchData",
                new
                {
                    controller = "administradores",
                    action = "switchData"
                }
            );
            routes.MapRoute(
                "AccesoGDE",
                "GDE",
                new
                {
                    controller = "administradores",
                    action = "obtenerCitaInsertada"
                }
            );
            routes.MapRoute(
                "AccesoCECA",
                "CECA/{idCita}",
                new
                {
                    controller = "administradores",
                    action = "ConsultarEstCitaAsoc"
                }
            );
            routes.MapRoute(
                "AccesoVPRE",
                "VPRE",
                new
                {
                    controller = "administradores",
                    action = "ValidarPrecita"
                }
            );
            routes.MapRoute(
                "AccesoGDC",
                "GDC/{idCita}",
                new
                {
                    controller = "administradores",
                    action = "ConsultarDescCita"
                }
            );

            routes.MapRoute(
                "Accesoestudiante",
                "estudiantes/estudiante/{carne}/{pin}",
                new
                {
                    controller = "estudiantes",
                    action = "estudiante",
                    carne= UrlParameter.Optional,
                    pin = UrlParameter.Optional
                }
            );
            routes.MapRoute(
            "AccesoEnfermedad",
            "Enfermedades/Enfermedad/{id}",
            new
            {
                controller = "Enfermedades",
                action = "Enfermedad",
                id = UrlParameter.Optional

            }
        );
            routes.MapRoute(
            "AccesoCE",
            "CE",
            new
            {
                controller = "Administradores",
                action = "cambioEstado",
 

            }
            );

            routes.MapRoute(
            "AccesoDelCita",
            "DC/{idC}",
                new
                {
                    controller = "Administradores",
                    action = "delCita",


                }
            );
            routes.MapRoute(
            "AccesoAdministradores",
            "Administradores",
            new
            {
                controller = "Administradores",
                action = "Administradores",
                id = UrlParameter.Optional

            }
            );
            routes.MapRoute(
                "AccesoAdministrador2",
                "Administradores/Administrador2/{cedula}",
                new
                {
                    controller = "Administradores",
                    action = "Administrador2"


                }
                );
         routes.MapRoute(
        "AccesoAdministrador",
        "Administradores/Administrador/{id}",
        new
        {
            controller = "Administradores",
            action = "Administrador"
       

        }
        );
         routes.MapRoute(
            "AccesoConfigCitaA",
            "ConfigCita/validarPreCitaUsuario/{carn}",
            new
            {
                controller = "Administradores",
                action = "ConfigCita"


            }
         );


         routes.MapRoute(
            "AccesoConfigCitaIns",
            "ConfigCita/InsertarCita/{carn}",
            new
            {
                controller = "Administradores",
                action = "ConfigCita"


            }
         );
         routes.MapRoute(
            "AccesoExClinico",
            "ExClinico/Insertar",  
            new
            {
                controller = "Administradores",
                action = "exClinico"


            }
         );
         routes.MapRoute(
            "AccesoExClinico2",
            "ExClinico/Actualizar",
            new
            {
                controller = "Administradores",
                action = "exClinico2"


            }
            );
         routes.MapRoute(
            "AccesoDientes",
            "DienteToExclinico/Insertar/{carne}/{tipo}",  
            new
            {
                controller = "Administradores",
                action = "opDientes"


            }
        );
         routes.MapRoute(
            "AccesoECFEGET",
            "ObtenerECFE/{carne}/{tipo}",  //hay que agregar esto al Trello
            new
            {
                controller = "Administradores",
                action = "opDientes"


            }
        );
         routes.MapRoute(
            "AccesoUpdateDiente",
            "UpdateDiente",  //hay que agregar esto al Trello
            new
            {
                controller = "Administradores",
                action = "opDientesUp"


            }
         );

         routes.MapRoute(
                "AccesoOpDiente",
                "searchExamenClinico/{carne}",  //hay que agregar esto al Trello
                new
                {
                    controller = "Administradores",
                    action = "opDientesUp"


                }
             );
         routes.MapRoute(
        "AccesoAdmHoras",
        "Horas/{cedula}",
        new
        {
            controller = "Administradores",
            action = "Administrador2"


        }
        );

        routes.MapRoute(
            "AccesoEnfermedad2",
            "Enfermedades2/Enfermedad2/{id}",
            new
            {
                controller = "Enfermedades",
                action = "Enfermedad2",
                id = UrlParameter.Optional

            }
        );
            routes.MapRoute(
                "Accesoestudiantes",
                "estudiantes",
                new
                {
                    controller = "estudiantes",
                    action = "estudiantes"
                }
            );
            routes.MapRoute(
                "AccesoSetNewEvent",
                "insertNewDate",
                new
                {
                    controller = "administradores",
                    action = "insertNewDate"
                }
            );
            routes.MapRoute(
                "AccesoEnfermedades",
                "Enfermedades",
                new
                {
                    controller = "Enfermedades",
                    action = "Enfermedades"
                }
            );
            routes.MapRoute(
                "AccesoEnfermedadesEst",
                "Enfermedades/Estudiante/{carne}",
                new
                {
                    controller = "estudiantes",
                    action = "estudiante",
             
                }
            );
            
            routes.MapRoute(
                "AccesoPTratamientos",
                "PTratamientos/{carne}",
                new
                {
                    controller = "Administradores",
                    action = "consultarPTratamiento"
                }
            );
            routes.MapRoute(
                "AccesoDates",
                "CitasFromId/{id}",
                new
                {
                    controller = "Administradores",
                    action = "CitasFromId"
                }
            );
            routes.MapRoute(
                "AccesoSesion",
                "Appoint-me/Log/{carne}",
                new
                {
                    controller = "AndroidClts",
                    action = "Sesion"
                }
            );
            routes.MapRoute(
                "AccesocitasControl",
                "citasControl/{fecha}/{tipo}",
                new
                {
                    controller = "Administradores",
                    action = "citasControl"
                }
            );
            routes.MapRoute(
                "AccesoIPTratamientos",
                "InsPTratamientos",
                new
                {
                    controller = "Administradores",
                    action = "insertarPTratamiento"
                }
            );

            routes.MapRoute(
                "AccesoCons_EC",
                "ConsultarEC/{idCita}",
                new
                {
                    controller = "Administradores",
                    action = "ConsultarEC"
                }
            );
            routes.MapRoute(
                "AccesoTratamientos",
                "Tratamientos",
                new
                {
                    controller = "Administradores",
                    action = "consultarTratamientos"
                }
            );
            routes.MapRoute(
                "AccesoAppointment",
                "Appointment/{carne}",
                new
                {
                    controller = "Administradores",
                    action = "getDateTimeAppointment"
                }
            );


            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index",
                    carne = UrlParameter.Optional,
                    pin = UrlParameter.Optional
                }
            );
        }
    }
}
