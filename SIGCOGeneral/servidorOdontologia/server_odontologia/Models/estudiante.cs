﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace server_odontologia.Models
{
    public class estudiante
    {
        public string carne { get; set; }
        public int pin { get; set; }
        public string cedula { get; set; }
        public string nombre { get; set; }
        public string ape1 { get; set; }
        public string ape2 { get; set; }
        public string carneCCSS { get; set; }
        public string telefono { get; set; }
        public string carrera { get; set; }
        public int becado { get; set; }
        public string sexo { get; set; }
        public int residente { get; set; }
        public int edad { get; set; }
        public string estadoCivil { get; set; }
        public string fechaNacimiento { get; set; }
    }
}