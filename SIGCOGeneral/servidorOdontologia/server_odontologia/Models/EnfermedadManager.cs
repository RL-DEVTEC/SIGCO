﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;

namespace server_odontologia.Models
{
    public class EnfermedadManager //Models views controllera  MVC4
    {
        private static string cadenaConexion = globals.IP;

        public bool InsertarEnfermedad(enfermedad cli)
        {
            SqlConnection con = new SqlConnection(cadenaConexion);

            con.Open();

            string sql = "INSERT INTO enfermedad (enfermedad,descripcion,tratamiento) VALUES (@enfermedad,@desc,@trat)";

            SqlCommand cmd = new SqlCommand(sql, con);
           
            cmd.Parameters.Add("@enfermedad", System.Data.SqlDbType.NVarChar).Value = cli.Enfermedad;
            cmd.Parameters.Add("@desc", System.Data.SqlDbType.NVarChar).Value = cli.descripcion;
            cmd.Parameters.Add("@trat", System.Data.SqlDbType.NVarChar).Value = cli.tratamiento;

            int res = cmd.ExecuteNonQuery();

            con.Close();

            return (res == 1);
        }

        public bool actualizarEnfermedad(enfermedad cli)
        {
            SqlConnection con = new SqlConnection(cadenaConexion);

            con.Open();

            string sql = "UPDATE enfermedad SET enfermedad=@enfermedad, descripcion=@desc, tratamiento=@trat where idEnfermedad=@id";
            
            SqlCommand cmd = new SqlCommand(sql, con);
            cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = cli.idEnfermedad;
            cmd.Parameters.Add("@enfermedad", System.Data.SqlDbType.NVarChar).Value = cli.Enfermedad;
            cmd.Parameters.Add("@desc", System.Data.SqlDbType.NVarChar).Value = cli.descripcion;
            cmd.Parameters.Add("@trat", System.Data.SqlDbType.NVarChar).Value = cli.tratamiento;

            int res = cmd.ExecuteNonQuery();

            con.Close();

            return (res == 1);
        }



        public bool ActualizarCliente(enfermedad cli)
        {
            SqlConnection con = new SqlConnection(cadenaConexion);

            con.Open();

            string sql = "UPDATE Clientes SET Nombre = @nombre, Telefono = @telefono WHERE IdCliente = @idcliente";

            SqlCommand cmd = new SqlCommand(sql, con);

            //cmd.Parameters.Add("@nombre", System.Data.SqlDbType.NVarChar).Value = cli.Nombre;
            //cmd.Parameters.Add("@telefono", System.Data.SqlDbType.NVarChar).Value = cli.Telefono;
            //cmd.Parameters.Add("@idcliente", System.Data.SqlDbType.Int).Value = cli.Id;

            int res = cmd.ExecuteNonQuery();

            con.Close();

            return (res == 1);
        }

        public bool eliminarEnfermedad(int id)
        {
          

            SqlConnection con = new SqlConnection(cadenaConexion);

            con.Open();

            string sql = "DELETE FROM enfermedad where idEnfermedad = @id";

            SqlCommand cmd = new SqlCommand(sql, con);

            cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = id;

            int res = cmd.ExecuteNonQuery();

            con.Close();

            return (res == 1);
        }

        public List<enfermedad> ObtenerEnfermedades()
        {
            List<enfermedad> lista = new List<enfermedad>();

            SqlConnection con = new SqlConnection(cadenaConexion);

            con.Open();

            string sql = "SELECT * from enfermedad" ;

            SqlCommand cmd = new SqlCommand(sql, con);

            SqlDataReader reader =
                cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);

            while (reader.Read())
            {
                enfermedad cli = new enfermedad();

                cli = new enfermedad();
                cli.idEnfermedad = reader.GetInt32(0);
                cli.Enfermedad = reader.GetString(1);
                cli.descripcion = reader.GetString(2);
                cli.tratamiento = reader.GetString(3);


                lista.Add(cli);
            }

            reader.Close();

            return lista;
        }

        public bool EliminarCliente(int id)
        {
            SqlConnection con = new SqlConnection(cadenaConexion);

            con.Open();

            string sql = "DELETE FROM Clientes WHERE IdCliente = @idcliente";

            SqlCommand cmd = new SqlCommand(sql, con);

            cmd.Parameters.Add("@idcliente", System.Data.SqlDbType.Int).Value = id;

            int res = cmd.ExecuteNonQuery();

            con.Close();

            return (res == 1);
        }
       

    }
}