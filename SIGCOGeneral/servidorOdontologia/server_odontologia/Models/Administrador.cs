﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace server_odontologia.Models
{
    public class Administrador
    {

        public string cedula { get; set; }
        public string nombre { get; set; }
        public string direccion { get; set; }
        public string ape1 { get; set; }
        public string ape2 { get; set; }
        public string email { get; set; }
        public string telefono { get; set; }
        public string contrasenna { get; set; }
        public string tipo { get; set; }


    }
}