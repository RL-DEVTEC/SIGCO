﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace server_odontologia.Models
{
    public class enfermedad
    {
        public int idEnfermedad { get; set; }
        public string Enfermedad { get; set; }
        public string descripcion { get; set; }
        public string tratamiento { get; set; }
    }
}