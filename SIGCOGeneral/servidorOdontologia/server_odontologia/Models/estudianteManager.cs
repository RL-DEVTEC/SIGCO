﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;

namespace server_odontologia.Models
{
    public class estudianteManager //Models views controllera  MVC4
    {
        private static string cadenaConexion = globals.IP;

        public List<enfermedad> getEnfermedadesEstudiante(string carne)
        {
            List<enfermedad> lista = new List<enfermedad>();
            SqlConnection con = new SqlConnection(cadenaConexion);

            con.Open();

            string sql = "SELECT E.idEnfermedad,E.enfermedad, E.descripcion, E.tratamiento from Estudiante AS est  INNER JOIN EnfermedadEstudiante as EE on (EE.carne=est.carne) INNER JOIN Enfermedad as E ON (E.idEnfermedad=EE.idEnfermedad) WHERE est.carne=@carne";

            SqlCommand cmd = new SqlCommand(sql, con);
            cmd.Parameters.Add("@carne", System.Data.SqlDbType.NVarChar).Value = carne;

            SqlDataReader reader =
                cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);

            while (reader.Read())
            {
                enfermedad enf = new enfermedad();
                enf.idEnfermedad = reader.GetInt32(0);
                enf.Enfermedad = reader.GetString(1);
                enf.descripcion = reader.GetString(2);
                enf.tratamiento = reader.GetString(3);

                //Cliente cli = new Cliente();

                //cli = new Cliente();
                //cli.Id = reader.GetInt32(0);
                //cli.Nombre = reader.GetString(1);
                //cli.Telefono = reader.GetString(2);

                lista.Add(enf);
            }

            reader.Close();

            return lista;
        }

        public bool ActualizarCliente(estudiante cli)
        {
            SqlConnection con = new SqlConnection(cadenaConexion);

            con.Open();

            string sql = "UPDATE Clientes SET Nombre = @nombre, Telefono = @telefono WHERE IdCliente = @idcliente";

            SqlCommand cmd = new SqlCommand(sql, con);

            //cmd.Parameters.Add("@nombre", System.Data.SqlDbType.NVarChar).Value = cli.Nombre;
            //cmd.Parameters.Add("@telefono", System.Data.SqlDbType.NVarChar).Value = cli.Telefono;
            //cmd.Parameters.Add("@idcliente", System.Data.SqlDbType.Int).Value = cli.Id;

            int res = cmd.ExecuteNonQuery();

            con.Close();

            return (res == 1);
        }


        public estudiante consultarLogin(string carne, int pin)
        {
            estudiante cli = null;

            SqlConnection con = new SqlConnection(cadenaConexion);

            con.Open();

            string sql = "select e.carne, e.pin, e.cedula,p.nombre,p.ape1, p.ape2,e.carneCCSS, e.carrera,e.becado, e.sexo, e.residente, e.edad, e.estadoCivil, e.fechaNacimiento ,tel.telefono from Estudiante as e INNER JOIN Persona as p on (e.cedula=p.cedula) LEFT OUTER JOIN Telefono as tel on(p.cedula=tel.cedula) WHERE e.carne=@carne";

            SqlCommand cmd = new SqlCommand(sql, con);

            cmd.Parameters.Add("@carne", System.Data.SqlDbType.NVarChar).Value = carne;

            SqlDataReader reader =
                cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
            
            if (reader.Read())
            {
                cli = new estudiante();
                cli.carne = reader.GetString(0);
                cli.pin = reader.GetInt32(1);
                cli.cedula = reader.GetString(2);
                cli.nombre = reader.GetString(3);
                cli.ape1 = reader.GetString(4);
                cli.ape2 = reader.GetString(5);
                cli.carneCCSS = reader.GetString(6);
                cli.carrera = reader.GetString(7);
                cli.becado = reader.GetInt32(8);
                cli.sexo = reader.GetString(9);
                cli.residente = reader.GetInt32(10);
                cli.edad = reader.GetInt32(11);
                cli.estadoCivil = reader.GetString(12);
                cli.fechaNacimiento = reader.GetDateTime(13).Day.ToString() + "-" + reader.GetDateTime(13).Month.ToString() + "-" + reader.GetDateTime(13).Year.ToString();
                cli.telefono = reader.GetString(14);

            }



            reader.Close();
            if (cli != null)
            {
                if (cli.carne.Equals(carne) && cli.pin.Equals(pin))
                {
                    return cli;
                }
                else
                {
                    cli = new estudiante();
                    cli.carne = "0";
                    cli.pin = 0;


                }

            }
            else
            {
                cli = new estudiante();
                cli.carne = "0";
                cli.pin = 0;

            }

            return cli;


        }
        public List<estudiante> ObtenerEstudiantes()
        {
            List<estudiante> lista = new List<estudiante>();

            SqlConnection con = new SqlConnection(cadenaConexion);

            con.Open();

            string sql = "select e.carne, e.pin, e.cedula,p.nombre,p.ape1, p.ape2,e.carneCCSS, e.carrera,e.becado, e.sexo, e.residente, e.edad, e.estadoCivil, e.fechaNacimiento ,tel.telefono from Estudiante as e INNER JOIN Persona as p on (e.cedula=p.cedula) LEFT OUTER JOIN Telefono as tel on(p.cedula=tel.cedula) ";

            SqlCommand cmd = new SqlCommand(sql, con);

            SqlDataReader reader =
                cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);

            while (reader.Read())
            {
                estudiante cli = new estudiante();
                cli.carne = reader.GetString(0);
                cli.pin = reader.GetInt32(1);
                cli.cedula = reader.GetString(2);
                cli.nombre = reader.GetString(3);
                cli.ape1 = reader.GetString(4);
                cli.ape2 = reader.GetString(5);
                cli.carneCCSS = reader.GetString(6);
                cli.carrera = reader.GetString(7);
                cli.becado = reader.GetInt32(8);
                cli.sexo = reader.GetString(9);
                cli.residente = reader.GetInt32(10);
                cli.edad = reader.GetInt32(11);
                cli.estadoCivil = reader.GetString(12);
                cli.fechaNacimiento = reader.GetDateTime(13).Day.ToString() + "-" + reader.GetDateTime(13).Month.ToString() + "-" + reader.GetDateTime(13).Year.ToString();
                try
                {
                    cli.telefono = reader.GetString(14);
                }
                catch (Exception ex)
                {
                    cli.telefono = "-";
                    string exd = ex.ToString();
                }
                lista.Add(cli);
            }

            reader.Close();

            return lista;
        }


        public estudiante ObtenerCliente(int id)
        {
            estudiante cli = null;

            SqlConnection con = new SqlConnection(cadenaConexion);

            con.Open();

            string sql = "SELECT Nombre, Telefono FROM Clientes WHERE IdCliente = @idcliente";

            SqlCommand cmd = new SqlCommand(sql, con);

            cmd.Parameters.Add("@idcliente", System.Data.SqlDbType.NVarChar).Value = id;

            SqlDataReader reader =
                cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);

            if (reader.Read())
            {
                cli = new estudiante();
                //cli.Id = id;
                //cli.Nombre = reader.GetString(0);
                //cli.Telefono = reader.GetString(1);
            }

            reader.Close();

            return cli;
        }



        public bool EliminarCliente(int id)
        {
            SqlConnection con = new SqlConnection(cadenaConexion);

            con.Open();

            string sql = "DELETE FROM Clientes WHERE IdCliente = @idcliente";

            SqlCommand cmd = new SqlCommand(sql, con);

            cmd.Parameters.Add("@idcliente", System.Data.SqlDbType.Int).Value = id;

            int res = cmd.ExecuteNonQuery();

            con.Close();

            return (res == 1);
        }
       

    }
}