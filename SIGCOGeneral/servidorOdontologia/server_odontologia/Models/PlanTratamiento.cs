﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace server_odontologia.Models
{
    public class PlanTratamiento
    {
        public string carne { get; set; }
        public string tratamiento { get; set; }
        public int diente { get; set; }
        public int seccion { get; set; }
        public string descripcion { get; set; }

    }
}