﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace server_odontologia.Models
{
    public class Cita_ID
    {

        public string carne { get; set; }

        public string nombre { get; set; }
        public string fecha { get; set; }
        public string horaI { get; set; }
        public string horaF { get; set; }

    }
}