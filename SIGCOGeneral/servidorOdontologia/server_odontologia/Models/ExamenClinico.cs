﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace server_odontologia.Models
{
    public class ExamenClinico
    {

        public int idExamen {get; set;}
        public string carne { get; set; }
        public string fecha { get; set; }
        public string descripcion { get; set; }
        public int tipo { get; set; }

    }
}