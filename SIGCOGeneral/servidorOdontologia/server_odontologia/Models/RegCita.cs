﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace server_odontologia.Models
{
    public class RegCita
    {

        public int id { get; set; }
        public string start { get; set; }

        public string end { get; set; }

        public string title { get; set; }

        public string state { get; set; }

  
    }
}