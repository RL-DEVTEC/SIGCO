﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace server_odontologia.Models
{
    public class Cita
    {
        public string carne { get; set; }
        public string fecha { get; set; }
        public string hora { get; set; }
    }
}