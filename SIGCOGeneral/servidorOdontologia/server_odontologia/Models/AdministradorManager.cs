﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using SnmpSharpNet;
using System.Net;

namespace server_odontologia.Models
{
    public class AdministradorManager //Models views controllera  MVC4
    {
        private static string cadenaConexion = globals.IP;

        public bool InsertarAdministrador(Administrador cli)
        {
            SqlConnection con = new SqlConnection(cadenaConexion);

            con.Open();

            string sql = "INSERT INTO Persona(cedula,nombre,direccion,ape1,ape2) VALUES (@ced,@nom,@direc,@ape1,@ape2) INSERT INTO Email(email,cedula) VALUES (@email,@ced) INSERT INTO Telefono(telefono,cedula) VALUES (@telefono,@ced) INSERT Administrador(contrasenna,tipo,cedula) VALUES (@contra,@tipo,@ced)";

            SqlCommand cmd = new SqlCommand(sql, con);
            cmd.Parameters.Add("@ced", System.Data.SqlDbType.NVarChar).Value = cli.cedula;
            cmd.Parameters.Add("@nom", System.Data.SqlDbType.NVarChar).Value = cli.nombre;
            cmd.Parameters.Add("@direc", System.Data.SqlDbType.NVarChar).Value = cli.direccion;
            cmd.Parameters.Add("@ape1", System.Data.SqlDbType.NVarChar).Value = cli.ape1;
            cmd.Parameters.Add("@ape2", System.Data.SqlDbType.NVarChar).Value = cli.ape2;
            cmd.Parameters.Add("@email", System.Data.SqlDbType.NVarChar).Value = cli.email;
            cmd.Parameters.Add("@telefono", System.Data.SqlDbType.NVarChar).Value = cli.telefono;
            cmd.Parameters.Add("@contra", System.Data.SqlDbType.NVarChar).Value = cli.contrasenna;
            cmd.Parameters.Add("@tipo", System.Data.SqlDbType.NVarChar).Value = cli.tipo;

            //cmd.Parameters.Add("@nombre", System.Data.SqlDbType.NVarChar).Value = cli.Nombre;
            //cmd.Parameters.Add("@telefono", System.Data.SqlDbType.NVarChar).Value = cli.Telefono;

            int res = cmd.ExecuteNonQuery();

            con.Close();

            return (res == 1);
        }
        public bool InsertarCita(Cita item)
        {
            SqlConnection con = new SqlConnection(cadenaConexion);

            con.Open();
            string sql = "EXEC InsertarNuevaCita @carne,@fecha,@hora";
            SqlCommand cmd = new SqlCommand(sql, con);
            cmd.Parameters.Add("@carne", System.Data.SqlDbType.NVarChar).Value = item.carne;
            cmd.Parameters.Add("@fecha", System.Data.SqlDbType.NVarChar).Value = item.fecha;
            cmd.Parameters.Add("@hora", System.Data.SqlDbType.NVarChar).Value = item.hora;

            int res = cmd.ExecuteNonQuery();

            con.Close();

            return (res == 1);
        }

        public string InsertarExamenClinico(ExamenClinico item)
        {
            SqlConnection con = new SqlConnection(cadenaConexion);
            

            con.Open();
            string sql = "";
            if (item.tipo.Equals(0))
            {

                sql = "EXEC insertGetExClinico @carne,0";// 0 PRIMER EXÁMEN
            }
            else{
                sql = "EXEC insertGetExClinico @carne,1";// 1 SEGUNDO O N EXÁMEN
            }
            SqlCommand cmd = new SqlCommand(sql, con);
            cmd.Parameters.Add("@carne", System.Data.SqlDbType.NVarChar).Value = item.carne;

            SqlDataReader reader =
                cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
            if (reader.Read())
            {
                return reader.GetString(0);

            }



            return "";
            /////
        }

        public int ConsultarEC(int idCita)
        {
            SqlConnection con = new SqlConnection(cadenaConexion);


            con.Open();
            string sql = "DBO.CONSULTAR_EC @ID_CITA";
            SqlCommand cmd = new SqlCommand(sql, con);
            cmd.Parameters.Add("@ID_CITA", System.Data.SqlDbType.Int).Value = idCita;

            SqlDataReader reader =
                cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
            if (reader.Read())
            {
                return reader.GetInt32(0);

            }
            return 3;



        }
        public bool InsertarDienteToExClinico(Diente item)
        {
            SqlConnection con = new SqlConnection(cadenaConexion);

            con.Open();
            string sql = "INSERT INTO Diente(idExamen,idDiente,extraccion,ausente,zona1,zona2,zona3,zona4,zona5)  VALUES(@idExamen,@idDiente,@extrac,@aus,@z1,@z2,@z3,@z4,@z5)";
            SqlCommand cmd = new SqlCommand(sql, con);
            cmd.Parameters.Add("@idExamen", System.Data.SqlDbType.Int).Value = item.idExamen;
            cmd.Parameters.Add("@idDiente", System.Data.SqlDbType.Int).Value = item.idDiente;
            cmd.Parameters.Add("@extrac", System.Data.SqlDbType.Int).Value = item.extraccion;
            cmd.Parameters.Add("@aus", System.Data.SqlDbType.Int).Value = item.ausente;
            cmd.Parameters.Add("@z1", System.Data.SqlDbType.Int).Value = item.zona1;
            cmd.Parameters.Add("@z2", System.Data.SqlDbType.Int).Value = item.zona2;
            cmd.Parameters.Add("@z3", System.Data.SqlDbType.Int).Value = item.zona3;
            cmd.Parameters.Add("@z4", System.Data.SqlDbType.Int).Value = item.zona4;
            cmd.Parameters.Add("@z5", System.Data.SqlDbType.Int).Value = item.zona5;
            int res = cmd.ExecuteNonQuery();

            con.Close();

            return (res == 1);
        }

        public bool ActualizarDienteFromExClinico(regDiente item)
        {

            if (item.ausente.Equals(0))
            {
                item.ausente = -1;
            }
            if (item.extraccion.Equals(0))
            {
                item.extraccion = -1;
            }
            SqlConnection con = new SqlConnection(cadenaConexion);

            con.Open();
            string sql = "EXEC ACTUALIZAR_DIENTE_EX_CLINICO_SEGUNDA_VEZ @carne,@idDiente,@extrac,@aus,@z1,@z2,@z3,@z4,@z5";
            SqlCommand cmd = new SqlCommand(sql, con);
            cmd.Parameters.Add("@carne", System.Data.SqlDbType.NVarChar).Value = item.carne;
            cmd.Parameters.Add("@idDiente", System.Data.SqlDbType.Int).Value = item.idDiente;
            cmd.Parameters.Add("@extrac", System.Data.SqlDbType.Int).Value = item.extraccion;
            cmd.Parameters.Add("@aus", System.Data.SqlDbType.Int).Value = item.ausente;
            cmd.Parameters.Add("@z1", System.Data.SqlDbType.Int).Value = item.zona1;
            cmd.Parameters.Add("@z2", System.Data.SqlDbType.Int).Value = item.zona2;
            cmd.Parameters.Add("@z3", System.Data.SqlDbType.Int).Value = item.zona3;
            cmd.Parameters.Add("@z4", System.Data.SqlDbType.Int).Value = item.zona4;
            cmd.Parameters.Add("@z5", System.Data.SqlDbType.Int).Value = item.zona5;
            int res = cmd.ExecuteNonQuery();

            con.Close();

            return (res == 1);
        }

        public bool ActualizarCliente(Administrador cli)
        {
            SqlConnection con = new SqlConnection(cadenaConexion);

            con.Open();

            string sql = "UPDATE Clientes SET Nombre = @nombre, Telefono = @telefono WHERE IdCliente = @idcliente";

            SqlCommand cmd = new SqlCommand(sql, con);

            //cmd.Parameters.Add("@nombre", System.Data.SqlDbType.NVarChar).Value = cli.Nombre;
            //cmd.Parameters.Add("@telefono", System.Data.SqlDbType.NVarChar).Value = cli.Telefono;
            //cmd.Parameters.Add("@idcliente", System.Data.SqlDbType.Int).Value = cli.Id;

            int res = cmd.ExecuteNonQuery();

            con.Close();

            return (res == 1);
        }

        public bool establecerCita(string fecha, string carne, string dia)
        {
            SqlConnection con = new SqlConnection(cadenaConexion);

            con.Open();

            string sql = "UPDATE Clientes SET Nombre = @nombre, Telefono = @telefono WHERE IdCliente = @idcliente";

            SqlCommand cmd = new SqlCommand(sql, con);

            //cmd.Parameters.Add("@nombre", System.Data.SqlDbType.NVarChar).Value = cli.Nombre;
            //cmd.Parameters.Add("@telefono", System.Data.SqlDbType.NVarChar).Value = cli.Telefono;
            //cmd.Parameters.Add("@idcliente", System.Data.SqlDbType.Int).Value = cli.Id;

            int res = cmd.ExecuteNonQuery();

            con.Close();

            return (res == 1);
        }



        public bool AsignarCita(string carne,string fecha,string dia)
        {


            SqlConnection con = new SqlConnection(cadenaConexion);

            con.Open();

            string sql = "EXEC actualizaAdm @cedPast,@ced,@nom,@direc,@ape1,@ape2,@email,@telefono,@contra,@tipo";



            SqlCommand cmd = new SqlCommand(sql, con);
            //cmd.Parameters.Add("@cedPast", System.Data.SqlDbType.NVarChar).Value = cedula;
            //cmd.Parameters.Add("@ced", System.Data.SqlDbType.NVarChar).Value = cli.cedula;
            //cmd.Parameters.Add("@nom", System.Data.SqlDbType.NVarChar).Value = cli.nombre;
            //cmd.Parameters.Add("@direc", System.Data.SqlDbType.NVarChar).Value = cli.direccion;
            //cmd.Parameters.Add("@ape1", System.Data.SqlDbType.NVarChar).Value = cli.ape1;
            //cmd.Parameters.Add("@ape2", System.Data.SqlDbType.NVarChar).Value = cli.ape2;
            //cmd.Parameters.Add("@email", System.Data.SqlDbType.NVarChar).Value = cli.email;
            //cmd.Parameters.Add("@telefono", System.Data.SqlDbType.NVarChar).Value = cli.telefono;
            //cmd.Parameters.Add("@contra", System.Data.SqlDbType.NVarChar).Value = cli.contrasenna;
            //cmd.Parameters.Add("@tipo", System.Data.SqlDbType.NVarChar).Value = cli.tipo;

            int res = cmd.ExecuteNonQuery();

            con.Close();

            return (res == 1);
        }

        public bool ActualizarAdministrador(string cedula, Administrador cli)
        {
          

            SqlConnection con = new SqlConnection(cadenaConexion);

            con.Open();

            string sql = "EXEC actualizaAdm @cedPast,@ced,@nom,@direc,@ape1,@ape2,@email,@telefono,@contra,@tipo";

 

            SqlCommand cmd = new SqlCommand(sql, con);
            cmd.Parameters.Add("@cedPast", System.Data.SqlDbType.NVarChar).Value = cedula;
            cmd.Parameters.Add("@ced", System.Data.SqlDbType.NVarChar).Value = cli.cedula;
            cmd.Parameters.Add("@nom", System.Data.SqlDbType.NVarChar).Value = cli.nombre;
            cmd.Parameters.Add("@direc", System.Data.SqlDbType.NVarChar).Value = cli.direccion;
            cmd.Parameters.Add("@ape1", System.Data.SqlDbType.NVarChar).Value = cli.ape1;
            cmd.Parameters.Add("@ape2", System.Data.SqlDbType.NVarChar).Value = cli.ape2;
            cmd.Parameters.Add("@email", System.Data.SqlDbType.NVarChar).Value = cli.email;
            cmd.Parameters.Add("@telefono", System.Data.SqlDbType.NVarChar).Value = cli.telefono;
            cmd.Parameters.Add("@contra", System.Data.SqlDbType.NVarChar).Value = cli.contrasenna;
            cmd.Parameters.Add("@tipo", System.Data.SqlDbType.NVarChar).Value = cli.tipo;

            int res = cmd.ExecuteNonQuery();

            con.Close();

            return (res == 1);
        }
        public String validarPreCitaUsuario(String carn)
        {
            
           // Result res = new Result();

            SqlConnection con = new SqlConnection(cadenaConexion);

            con.Open();

            string sql = "EXEC obtenerPermiso @carne";

            SqlCommand cmd = new SqlCommand(sql, con);
            cmd.Parameters.Add("@carne", System.Data.SqlDbType.NVarChar).Value = carn;

            SqlDataReader reader =
                cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
            if (reader.Read())
            {
                return reader.GetString(0);

            }

            reader.Close();

            return "";
        }

        public Appointment getAppointment(string carne)
        {
            Appointment cli = null;

            SqlConnection con = new SqlConnection(cadenaConexion);

            con.Open();

            string sql = "DBO.GET_APPOINTMENT @CARNE";

            SqlCommand cmd = new SqlCommand(sql, con);

            cmd.Parameters.Add("@CARNE", System.Data.SqlDbType.NVarChar).Value = carne;

            SqlDataReader reader =
                cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);

            if (reader.Read())
            {
                cli = new Appointment();
                cli.fecha = reader.GetDateTime(0).Day.ToString() + "/" + reader.GetDateTime(0).Month.ToString() + "/" + reader.GetDateTime(0).Year.ToString();
                cli.hora = reader.GetTimeSpan(1).Hours.ToString() + ":" + reader.GetTimeSpan(1).Minutes.ToString() + ":" + reader.GetTimeSpan(1).Seconds.ToString();


            }

            reader.Close();

            return cli; //insertNewDate
        }

        public int insertNewDate(string carne, string fecha, string horaI, string horaF,int tipo, int preID,string descr)
        {


            SqlConnection con = new SqlConnection(cadenaConexion);

            con.Open();

            string sql = "DBO.CONSULTAR_CREAR_CITA @CARNE, @FECHA, @HORAI, @HORAF, @TIPO, @ID_CITA_INHAB, @DESCR";



            SqlCommand cmd = new SqlCommand(sql, con);
            cmd.Parameters.Add("@CARNE", System.Data.SqlDbType.NVarChar).Value = carne;
            cmd.Parameters.Add("@FECHA", System.Data.SqlDbType.NVarChar).Value = fecha;
            cmd.Parameters.Add("@HORAI", System.Data.SqlDbType.NVarChar).Value = horaI;
            cmd.Parameters.Add("@HORAF", System.Data.SqlDbType.NVarChar).Value = horaF;
            cmd.Parameters.Add("@TIPO", System.Data.SqlDbType.Int).Value = tipo;
            cmd.Parameters.Add("@ID_CITA_INHAB", System.Data.SqlDbType.Int).Value = preID;
            cmd.Parameters.Add("@DESCR", System.Data.SqlDbType.NVarChar).Value = descr;



            SqlDataReader reader =
                cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
            if (reader.Read())
            {
                return reader.GetInt32(0);

            }

            reader.Close();

            return 0;
        }

        public String searchExamenClinico(String carne)
        {

            // Result res = new Result();

            SqlConnection con = new SqlConnection(cadenaConexion);

            con.Open();

            string sql = "DBO.searchExamenClinico @carne";

            SqlCommand cmd = new SqlCommand(sql, con);
            cmd.Parameters.Add("@carne", System.Data.SqlDbType.NVarChar).Value = carne;

            SqlDataReader reader =
                cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
            if (reader.Read())
            {
                return reader.GetString(0);

            }

            reader.Close();

            return "";
        }

        public List<Switch> consultarDatosSwitch()
        {
            List<Switch> lista = new List<Switch>();

            OctetString community = new OctetString("redes");
            AgentParameters param = new AgentParameters(community);
            param.Version = SnmpVersion.Ver2;
            IpAddress agent = new IpAddress("172.24.6.11");
            UdpTarget target = new UdpTarget((IPAddress)agent, 161, 1000, 1);
            Pdu pdu = new Pdu(PduType.Get);
            //SnmpV2Packet result = (SnmpV2Packet)target.Request(pdu, param);



            pdu.VbList.Add("1.3.6.1.2.1.2.2.1.8.10001");
            pdu.VbList.Add("1.3.6.1.2.1.2.2.1.8.10002");
            pdu.VbList.Add("1.3.6.1.2.1.2.2.1.8.10003");
            pdu.VbList.Add("1.3.6.1.2.1.2.2.1.8.10004");
            pdu.VbList.Add("1.3.6.1.2.1.2.2.1.8.10005");
            pdu.VbList.Add("1.3.6.1.2.1.2.2.1.8.10006");
            pdu.VbList.Add("1.3.6.1.2.1.2.2.1.8.10007");
            pdu.VbList.Add("1.3.6.1.2.1.2.2.1.8.10008");
            pdu.VbList.Add("1.3.6.1.2.1.2.2.1.8.10009");
            pdu.VbList.Add("1.3.6.1.2.1.2.2.1.8.10010");
            pdu.VbList.Add("1.3.6.1.2.1.2.2.1.8.10011");
            pdu.VbList.Add("1.3.6.1.2.1.2.2.1.8.10012");
            pdu.VbList.Add("1.3.6.1.2.1.2.2.1.8.10013");
            pdu.VbList.Add("1.3.6.1.2.1.2.2.1.8.10014");
            pdu.VbList.Add("1.3.6.1.2.1.2.2.1.8.10015");
            pdu.VbList.Add("1.3.6.1.2.1.2.2.1.8.10016");
            pdu.VbList.Add("1.3.6.1.2.1.2.2.1.8.10017");
            pdu.VbList.Add("1.3.6.1.2.1.2.2.1.8.10018");
            pdu.VbList.Add("1.3.6.1.2.1.2.2.1.8.10019");
            pdu.VbList.Add("1.3.6.1.2.1.2.2.1.8.10020");
            pdu.VbList.Add("1.3.6.1.2.1.2.2.1.8.10021");
            pdu.VbList.Add("1.3.6.1.2.1.2.2.1.8.10022");
            pdu.VbList.Add("1.3.6.1.2.1.2.2.1.8.10023");
            pdu.VbList.Add("1.3.6.1.2.1.2.2.1.8.10024");


            SnmpV2Packet result = (SnmpV2Packet)target.Request(pdu, param);

            for (int i = 0; i < 24; i++)
            {
                Switch aux = new Switch();
                aux.estado = result.Pdu.VbList[i].Value.ToString();
                aux.puerto = i + 1;
                lista.Add(aux);
            }

            return lista;
        }

        //

        public String ConsultarEstCitaAsoc(int idCita)
        {

            // Result res = new Result();

            SqlConnection con = new SqlConnection(cadenaConexion);

            con.Open();

            string sql = "DBO.CONSULTAR_EST_CITA_ASOC @ID_CITA";

            SqlCommand cmd = new SqlCommand(sql, con);
            cmd.Parameters.Add("@ID_CITA", System.Data.SqlDbType.Int).Value = idCita;

            SqlDataReader reader =
                cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
            if (reader.Read())
            {
                return reader.GetString(0);

            }

            reader.Close();

            return "";
        }
        public int obtenerCitaInsertada(string horaI, string horaF, string fecha)
        {

            // Result res = new Result();

            SqlConnection con = new SqlConnection(cadenaConexion);

            con.Open();

            string sql = "DBO.GET_DATE_INSERTED @HORAI,@HORAF,@FECHA";

            SqlCommand cmd = new SqlCommand(sql, con);
            cmd.Parameters.Add("@HORAI", System.Data.SqlDbType.NVarChar).Value = horaI;
            cmd.Parameters.Add("@HORAF", System.Data.SqlDbType.NVarChar).Value = horaF;
            cmd.Parameters.Add("@FECHA", System.Data.SqlDbType.NVarChar).Value = fecha;

            SqlDataReader reader =
                cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
            if (reader.Read())
            {
                return reader.GetInt32(0);

            }

            reader.Close();

            return 0;
        }
        public String ValidarPrecita(string horaI, string horaF, string fecha)
        {

            // Result res = new Result();obtenerCitaInsertada

            SqlConnection con = new SqlConnection(cadenaConexion);

            con.Open();

            string sql = "DBO.VALIDAR_PRECITA @HORAI,@HORAF,@FECHA";

            SqlCommand cmd = new SqlCommand(sql, con);
            cmd.Parameters.Add("@HORAI", System.Data.SqlDbType.NVarChar).Value = horaI;
            cmd.Parameters.Add("@HORAF", System.Data.SqlDbType.NVarChar).Value = horaF;
            cmd.Parameters.Add("@FECHA", System.Data.SqlDbType.NVarChar).Value = fecha;

            SqlDataReader reader =
                cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
            if (reader.Read())
            {
                return reader.GetInt32(0).ToString();

            }

            reader.Close();

            return "";
        }
        public String ConsultarDescCita(int idCita)
        {

            // Result res = new Result();

            SqlConnection con = new SqlConnection(cadenaConexion);

            con.Open();

            string sql = "DBO.CONSULTAR_DESC_CITA_INHAB @ID_CITA";

            SqlCommand cmd = new SqlCommand(sql, con);
            cmd.Parameters.Add("@ID_CITA", System.Data.SqlDbType.Int).Value = idCita;

            SqlDataReader reader =
                cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
            if (reader.Read())
            {
                return reader.GetString(0);

            }

            reader.Close();

            return "";
        }

        public List<Cita_ID> getCitasFromId(int id)
        {
            List<Cita_ID> lista = new List<Cita_ID>();

            SqlConnection con = new SqlConnection(cadenaConexion);

            con.Open();

            string sql = "DBO.GET_DATE @id";

            SqlCommand cmd = new SqlCommand(sql, con);
            cmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = id;

            SqlDataReader reader =
                cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);

            while (reader.Read())
            {
                Cita_ID cli = new Cita_ID();

                cli.carne = reader.GetString(0).ToString();
                cli.nombre = reader.GetString(1).ToString();
                cli.fecha = reader.GetDateTime(2).Day.ToString() + "-" + reader.GetDateTime(2).Month + "-" + reader.GetDateTime(2).Year.ToString();
                cli.horaI = reader.GetTimeSpan(3).Hours.ToString() + ":" + reader.GetTimeSpan(3).Minutes.ToString() + ":" + reader.GetTimeSpan(3).Seconds.ToString();
                cli.horaF = reader.GetTimeSpan(4).Hours.ToString() + ":" + reader.GetTimeSpan(4).Minutes.ToString() + ":" + reader.GetTimeSpan(4).Seconds.ToString();

                lista.Add(cli);
            }

            reader.Close();

            return lista;
        }
        public List<RegCita> getControlCitas(String fecha, int tipo)
        {
            List<RegCita> lista = new List<RegCita>();

            SqlConnection con = new SqlConnection(cadenaConexion);

            con.Open();

            string sql = "DBO.OBTENER_CITAS_FECHA @fecha, @tipo";

            SqlCommand cmd = new SqlCommand(sql, con);
            cmd.Parameters.Add("@fecha", System.Data.SqlDbType.NVarChar).Value = fecha;
            cmd.Parameters.Add("@tipo", System.Data.SqlDbType.Int).Value = tipo;

            SqlDataReader reader =
                cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);

            while (reader.Read())
            {
                RegCita cli = new RegCita();

                cli.id = reader.GetInt32(2);
                string end = "";
                string inicio = "";
                inicio += reader.GetString(7).ToString() + " ";
               
                inicio += reader.GetString(8).ToString()+" ";
                inicio += reader.GetDateTime(1).Day.ToString() + " ";
                inicio += reader.GetDateTime(1).Year.ToString() + " ";
                inicio += reader.GetTimeSpan(5).Hours.ToString()+":";
                inicio += reader.GetTimeSpan(5).Minutes.ToString() + ":";
                inicio += reader.GetTimeSpan(5).Seconds.ToString() + " ";
                inicio += " GMT-0600 (Hora estándar, América Central)";

                end += reader.GetString(7).ToString() + " ";

                end += reader.GetString(8).ToString() + " ";
                end += reader.GetDateTime(1).Day.ToString() + " ";
                end += reader.GetDateTime(1).Year.ToString() + " ";
                end += reader.GetTimeSpan(6).Hours.ToString() + ":";
                end += reader.GetTimeSpan(6).Minutes.ToString() + ":";
                end += reader.GetTimeSpan(6).Seconds.ToString() + " ";
                end += " GMT-0600 (Hora estándar, América Central)";
                cli.start = inicio;
                cli.end = end;

                cli.state = reader.GetString(9).ToString();

                cli.title = reader.GetString(4).ToString();

               

                lista.Add(cli);
            }

            reader.Close();

            return lista;
        }
        public List<Hora> HoursFromDate(String date)
        {
            List<Hora> lista = new List<Hora>();

            SqlConnection con = new SqlConnection(cadenaConexion);

            con.Open();

            string sql = "DBO.HOUR_FROM_DATE @FECHA";

            SqlCommand cmd = new SqlCommand(sql, con);
            cmd.Parameters.Add("@FECHA", System.Data.SqlDbType.NVarChar).Value = date;

            SqlDataReader reader =
                cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);

            while (reader.Read())
            {
                Hora cli = new Hora();


                cli.hora = reader.GetTimeSpan(0).Hours.ToString() + ":" + reader.GetTimeSpan(0).Minutes.ToString();
                lista.Add(cli);
            }

            reader.Close();

            return lista;
        }
        public List<Diente> ObtenerExCliFromEst(string carne,string tipo)
        {
            List<Diente> lista = new List<Diente>();

            SqlConnection con = new SqlConnection(cadenaConexion);

            con.Open();

            string sql = "OBTENER_EX_CLI_FROM_EST @CARNE,@TIPO";

            SqlCommand cmd = new SqlCommand(sql, con);

            cmd.Parameters.Add("@CARNE", System.Data.SqlDbType.NVarChar).Value = carne;
            cmd.Parameters.Add("@TIPO", System.Data.SqlDbType.Int).Value = tipo;

            SqlDataReader reader =
                cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);

            while (reader.Read())
            {
                Diente cli = new Diente();
                cli.idExamen = reader.GetInt32(0);
                cli.idDiente = reader.GetInt32(1);
                cli.extraccion = reader.GetInt32(2);
                cli.ausente = reader.GetInt32(3);
                cli.zona1 = reader.GetInt32(4);
                cli.zona2 = reader.GetInt32(5);
                cli.zona3 = reader.GetInt32(6);
                cli.zona4 = reader.GetInt32(7);
                cli.zona5 = reader.GetInt32(8);
                lista.Add(cli);
            }

            reader.Close();

            return lista;
        }

        public List<PTratamiento> consultPlanesTratamiento(string carne)
        {
            List<PTratamiento> lista = new List<PTratamiento>();

            SqlConnection con = new SqlConnection(cadenaConexion);

            con.Open();

            string sql = "DBO.CONSULTAR_PTRATAMIENTO_EST @carne ";

            SqlCommand cmd = new SqlCommand(sql, con);
            cmd.Parameters.Add("@carne", System.Data.SqlDbType.NVarChar).Value = carne;


            SqlDataReader reader =
                cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);

            while (reader.Read())
            {
                PTratamiento cli = new PTratamiento();
                cli.fecha = reader.GetDateTime(0).Day.ToString() + "-" + reader.GetDateTime(0).Month.ToString() + "-" + reader.GetDateTime(0).Year.ToString();
                cli.tratamiento = reader.GetString(1);
                cli.pieza = reader.GetInt32(2);
                cli.id = reader.GetInt32(3);
                cli.desc = reader.GetString(4);
                lista.Add(cli);
            }

            reader.Close();

            return lista;
        }
        public List<Tratamiento> consultTratamientos()
        {
            List<Tratamiento> lista = new List<Tratamiento>();

            SqlConnection con = new SqlConnection(cadenaConexion);

            con.Open();

            string sql = "DBO.OBTENER_TRATAMIENTOS";

            SqlCommand cmd = new SqlCommand(sql, con);


            SqlDataReader reader =
                cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);

            while (reader.Read())
            {
                Tratamiento cli = new Tratamiento();
                
                cli.tratamiento = reader.GetString(0);
               
                lista.Add(cli);
            }

            reader.Close();

            return lista;
        }

        public List<Administrador> ObtenerAdministradores()
        {
            List<Administrador> lista = new List<Administrador>();

            SqlConnection con = new SqlConnection(cadenaConexion);

            con.Open();

            string sql = "DBO.OBTENER_ADMINISTRADORES";

            SqlCommand cmd = new SqlCommand(sql, con);

            SqlDataReader reader =
                cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);

            while (reader.Read())
            {
                Administrador cli = new Administrador();

                cli = new Administrador();
                cli.cedula = reader.GetString(0);
                cli.nombre = reader.GetString(1);
                cli.ape1 = reader.GetString(2);
                cli.ape2 = reader.GetString(3);
                cli.direccion = reader.GetString(4);
                cli.email = reader.GetString(5);
                cli.telefono = reader.GetString(6);
                cli.contrasenna = reader.GetString(7);
                cli.tipo = reader.GetString(8);
                lista.Add(cli);
            }

            reader.Close();

            return lista;
        }
        public bool cambiarEstado(Desc desc)
        {
            SqlConnection con = new SqlConnection(cadenaConexion);

            con.Open();

            string sql = "DBO.CAMBIAR_ESTADO_CITA @ID_CITA, @ESTADO, @DESCRIP";

            SqlCommand cmd = new SqlCommand(sql, con);

            cmd.Parameters.Add("@ID_CITA", System.Data.SqlDbType.Int).Value = desc.id;
            cmd.Parameters.Add("@ESTADO", System.Data.SqlDbType.NVarChar).Value = desc.estado;
            cmd.Parameters.Add("@DESCRIP", System.Data.SqlDbType.NVarChar).Value = desc.descrip;

            int res = cmd.ExecuteNonQuery();

            con.Close();

            return (res == 1);
        }
        public bool delCitaFromId(int idC)
        {
            SqlConnection con = new SqlConnection(cadenaConexion);

            con.Open();

            string sql = "EXEC DEL_CITA_FROM_ID @idC";

            SqlCommand cmd = new SqlCommand(sql, con);

            cmd.Parameters.Add("@idC", System.Data.SqlDbType.Int).Value = idC;

            int res = cmd.ExecuteNonQuery();

            con.Close();

            return (res == 1);
        }


        public bool EliminarAdministrador(string cedula)
        {
            SqlConnection con = new SqlConnection(cadenaConexion);

            con.Open();

            string sql = "DELETE FROM Administrador WHERE cedula = @cedula";

            SqlCommand cmd = new SqlCommand(sql, con);

            cmd.Parameters.Add("@cedula", System.Data.SqlDbType.NVarChar).Value = cedula;

            int res = cmd.ExecuteNonQuery();

            con.Close();

            return (res == 1);
        }
        public bool insertarPlanTratamiento(PlanTratamiento obj)
        {
            SqlConnection con = new SqlConnection(cadenaConexion);

            con.Open();

            string sql = "DBO.INSERTAR_PLAN_TRATAMIENTO @CARNE,@TRATAMIENTO,@DIENTE,@SECCION,@DESCRIPCION";

            SqlCommand cmd = new SqlCommand(sql, con);

            cmd.Parameters.Add("@CARNE", System.Data.SqlDbType.NVarChar).Value = obj.carne;
            cmd.Parameters.Add("@TRATAMIENTO", System.Data.SqlDbType.NVarChar).Value = obj.tratamiento;
            cmd.Parameters.Add("@DIENTE", System.Data.SqlDbType.Int).Value = obj.diente;
            cmd.Parameters.Add("@SECCION", System.Data.SqlDbType.Int).Value = obj.seccion;
            cmd.Parameters.Add("@DESCRIPCION", System.Data.SqlDbType.NVarChar).Value = obj.descripcion;


            int res = cmd.ExecuteNonQuery();

            con.Close();

            return (res == 1);
        }
        public bool ActualizarDiente(Diente item)
        {
            SqlConnection con = new SqlConnection(cadenaConexion);

            con.Open();

            string sql = "DBO.ACTUALIZAR_DIENTE @EXTRAC, @AUS, @Z1, @Z2, @Z3, @Z4, @Z5, @ID_EXAMEN, @ID_DIENTE";

            SqlCommand cmd = new SqlCommand(sql, con);

            cmd.Parameters.Add("@EXTRAC", System.Data.SqlDbType.Int).Value =item.extraccion;
            cmd.Parameters.Add("@AUS", System.Data.SqlDbType.Int).Value = item.ausente;
            cmd.Parameters.Add("@Z1", System.Data.SqlDbType.Int).Value = item.zona1;
            cmd.Parameters.Add("@Z2", System.Data.SqlDbType.Int).Value = item.zona2;
            cmd.Parameters.Add("@Z3", System.Data.SqlDbType.Int).Value = item.zona3;
            cmd.Parameters.Add("@Z4", System.Data.SqlDbType.Int).Value = item.zona4;
            cmd.Parameters.Add("@Z5", System.Data.SqlDbType.Int).Value = item.zona5;
            cmd.Parameters.Add("@ID_EXAMEN", System.Data.SqlDbType.Int).Value = item.idExamen;
            cmd.Parameters.Add("@ID_DIENTE", System.Data.SqlDbType.Int).Value = item.idDiente;

            int res = cmd.ExecuteNonQuery();

            con.Close();

            return (res == 1);
        }
       

    }
}