﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace server_odontologia.Models
{
    public class Diente
    {

        public int idExamen { get; set; }
        public int idDiente { get; set; }
        public int extraccion { get; set; }
        public int ausente { get; set; }
        public int zona1 { get; set; }
        public int zona2 { get; set; }
        public int zona3 { get; set; }
        public int zona4 { get; set; }
        public int zona5 { get; set; }

    }
}