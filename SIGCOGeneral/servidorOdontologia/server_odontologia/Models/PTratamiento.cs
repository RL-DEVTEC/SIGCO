﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace server_odontologia.Models
{
    public class PTratamiento
    {

        public string fecha { get; set; }
        public string tratamiento { get; set; }

        public int pieza { get; set; }
        public int id { get; set; }
        public string desc { get; set; }

    }
}