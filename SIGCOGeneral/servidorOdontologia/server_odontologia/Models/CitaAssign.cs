﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace server_odontologia.Models
{
    public class CitaAssign
    {

        public string carne { get; set; }
        public string fecha { get; set; }
        public string horaI { get; set; }
        public string horaF { get; set; }
        public int tipo { get; set; }
        public int preID { get; set; }
        public string descr { get; set; }
    }
}