﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using server_odontologia.Models;

namespace server_odontologia.Controllers
{
    public class AndroidCltsController : Controller
    {
        private AndroidCltManager androidCltsManager;

        public AndroidCltsController()
        {
            androidCltsManager = new AndroidCltManager();
        }

        // GET /Api/Clientes
       /* [HttpGet]
        public JsonResult AndroidClts()
        {
            return Json(androidCltsManager.ObtenerClientes(), 
                        JsonRequestBehavior.AllowGet);
        }*/

        // POST    Clientes/Cliente    { Nombre:"nombre", Telefono:123456789 }
        // PUT     Clientes/Cliente/3  { Id:3, Nombre:"nombre", Telefono:123456789 }
        // GET     Clientes/Cliente/3
        // DELETE  Clientes/Cliente/3
        public JsonResult Sesion(string carne, Sesion item)
        {
            switch (Request.HttpMethod)
            {
              /*  case "POST":
                    return Json(androidCltsManager.InsertarCliente(item));
                case "PUT":
                    return Json(androidCltsManager.ActualizarCliente(item));*/
                case "GET":
                    return Json(androidCltsManager.consultarSesion(carne), 
                                JsonRequestBehavior.AllowGet);
              //  case "DELETE":
                   // return Json(androidCltsManager.EliminarCliente(id.GetValueOrDefault()));
            }

            return Json(new { Error = true, Message = "Operación HTTP desconocida" });
        }
    }

    }

