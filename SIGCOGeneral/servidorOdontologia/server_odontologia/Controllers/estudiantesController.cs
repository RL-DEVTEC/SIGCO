﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using server_odontologia.Models;

namespace server_odontologia.Controllers
{
    public class estudiantesController : Controller
    {
        private estudianteManager estudiantesManager;

        public estudiantesController()
        {
            estudiantesManager = new estudianteManager();
        }

        // GET /Api/Clientes
        [HttpGet]
        public JsonResult estudiantes()
        {
            return Json(estudiantesManager.ObtenerEstudiantes(), 
                        JsonRequestBehavior.AllowGet);
        }

        // POST    Clientes/Cliente    { Nombre:"nombre", Telefono:123456789 }
        // PUT     Clientes/Cliente/3  { Id:3, Nombre:"nombre", Telefono:123456789 }
        // GET     Clientes/Cliente/3
        // DELETE  Clientes/Cliente/3
        public JsonResult estudiante(string carne,int? pin, estudiante item)
        {
            switch (Request.HttpMethod)
            {
                case "POST":
                    return Json(estudiantesManager.getEnfermedadesEstudiante(carne));
                case "PUT":
                    return Json(estudiantesManager.ActualizarCliente(item));
                case "GET":
                    return Json(estudiantesManager.consultarLogin(carne,pin.GetValueOrDefault()), 
                                JsonRequestBehavior.AllowGet);
                case "DELETE":
                    return Json(estudiantesManager.EliminarCliente(pin.GetValueOrDefault()));
            }

            return Json(new { Error = true, Message = "Operación HTTP desconocida" });
        }
    }

    }

