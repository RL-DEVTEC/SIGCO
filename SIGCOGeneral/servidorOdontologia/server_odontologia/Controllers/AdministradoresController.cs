﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using server_odontologia.Models;

namespace server_odontologia.Controllers
{
    public class AdministradoresController : Controller
    {
        private AdministradorManager administradoresManager;

        public AdministradoresController()
        {
            administradoresManager = new AdministradorManager();
        }

        // GET /Api/Clientes
        [HttpGet]
        public JsonResult Administradores()
        {
            return Json(administradoresManager.ObtenerAdministradores(), 
                        JsonRequestBehavior.AllowGet);
        }

        // POST    Clientes/Cliente    { Nombre:"nombre", Telefono:123456789 }
        // PUT     Clientes/Cliente/3  { Id:3, Nombre:"nombre", Telefono:123456789 }
        // GET     Clientes/Cliente/3
        // DELETE  Clientes/Cliente/3
        public JsonResult Administrador(string cedula, Administrador item)
        {
            switch (Request.HttpMethod)
            {
                case "POST":
                    return Json(administradoresManager.InsertarAdministrador(item));
               case "PUT":
                    return Json(administradoresManager.ActualizarCliente(item));
                case "GET":
                    return Json(administradoresManager.ActualizarAdministrador(cedula,item), // se va a guardar esta para actualizar un Adm
                                JsonRequestBehavior.AllowGet);
                /*   case "DELETE":
                      return Json(administradoresManager.EliminarCliente(id.GetValueOrDefault()));*/
            }

            return Json(new { Error = true, Message = "Operación HTTP desconocida" });
        }  //cambioEstado

        public JsonResult cambioEstado(Desc desc)
        {
            switch (Request.HttpMethod)
            {
                case "POST":
                    return Json(administradoresManager.cambiarEstado(desc));
            }

            return Json(new { Error = true, Message = "Operación HTTP desconocida" });
        }
        public JsonResult delCita(int idC)
        {
            switch (Request.HttpMethod)
            {
                case "GET":
                    return Json(administradoresManager.delCitaFromId(idC), JsonRequestBehavior.AllowGet);
            }

            return Json(new { Error = true, Message = "Operación HTTP desconocida" });
        }


        public JsonResult citasControl(string fecha,int tipo, RegCita item)
        {
            switch (Request.HttpMethod)
            {
                case "GET":
                    return Json(administradoresManager.getControlCitas(fecha,tipo), // se va a guardar esta para actualizar un Adm
                                JsonRequestBehavior.AllowGet);
            }

            return Json(new { Error = true, Message = "Operación HTTP desconocida" });
        }
        public JsonResult CitasFromId(int id, RegCita item)
        {
            switch (Request.HttpMethod)
            {
                case "GET":
                    return Json(administradoresManager.getCitasFromId(id), 
                                JsonRequestBehavior.AllowGet);
            }

            return Json(new { Error = true, Message = "Operación HTTP desconocida" }); 
        }
        public JsonResult obtenerCitaInsertada(VPRE item)
        {
            switch (Request.HttpMethod)
            {
                case "GET":
                    return Json(administradoresManager.obtenerCitaInsertada(item.horaI, item.horaF, item.fecha),
                                JsonRequestBehavior.AllowGet);
            }

            return Json(new { Error = true, Message = "Operación HTTP desconocida" });  //ConsultarDescCita
        }
        public JsonResult ValidarPrecita(VPRE item)
        {
            switch (Request.HttpMethod)
            {
                case "GET":
                    return Json(administradoresManager.ValidarPrecita(item.horaI,item.horaF,item.fecha),
                                JsonRequestBehavior.AllowGet);
            }

            return Json(new { Error = true, Message = "Operación HTTP desconocida" });  //ConsultarDescCita
        }
        public JsonResult ConsultarDescCita(int idCita, RegCita item)
        {
            switch (Request.HttpMethod)
            {
                case "GET":
                    return Json(administradoresManager.ConsultarDescCita(idCita),
                                JsonRequestBehavior.AllowGet);
            }

            return Json(new { Error = true, Message = "Operación HTTP desconocida" });  //ConsultarDescCita
        } //switchData

        public JsonResult switchData(Switch item)
        {
            switch (Request.HttpMethod)
            {
                case "GET":
                    return Json(administradoresManager.consultarDatosSwitch(),
                                JsonRequestBehavior.AllowGet);
            }

            return Json(new { Error = true, Message = "Operación HTTP desconocida" });  //ConsultarDescCita
        }

        public JsonResult ConsultarEstCitaAsoc(int idCita, RegCita item)
        {
            switch (Request.HttpMethod)
            {
                case "GET":
                    return Json(administradoresManager.ConsultarEstCitaAsoc(idCita),
                                JsonRequestBehavior.AllowGet);
            }

            return Json(new { Error = true, Message = "Operación HTTP desconocida" });  //ConsultarDescCita
        }
        
        public JsonResult ConsultarEC(int idCita)
        {
            switch (Request.HttpMethod)
            {
                case "GET":
                    return Json(administradoresManager.ConsultarEC(idCita),
                                JsonRequestBehavior.AllowGet);
            }

            return Json(new { Error = true, Message = "Operación HTTP desconocida" });
        }
        public JsonResult insertNewDate(CitaAssign item)
        {
            switch (Request.HttpMethod)
            {
                case "POST":
                    return Json(administradoresManager.insertNewDate(item.carne, item.fecha, item.horaI, item.horaF, item.tipo, item.preID,item.descr));
            }

            return Json(new { Error = true, Message = "Operación HTTP desconocida" });
        }
        
        public JsonResult Administrador2(string cedula, Administrador item)
        {
            switch (Request.HttpMethod)
            {
                case "POST":
                    return Json(administradoresManager.EliminarAdministrador(cedula));
                case "PUT":
                    return Json(administradoresManager.ActualizarCliente(item));
                case "GET":
                    return Json(administradoresManager.HoursFromDate(cedula),
                                JsonRequestBehavior.AllowGet);
                /*   case "DELETE":
                      return Json(administradoresManager.EliminarCliente(id.GetValueOrDefault()));*/
            }

            return Json(new { Error = true, Message = "Operación HTTP desconocida" });
        }
        public JsonResult getDateTimeAppointment(string carne)
        {
            switch (Request.HttpMethod)
            {
                // case "POST":
                //  return Json(resultsManager.ObtenerClt(user, pass));
                case "GET":
                    return Json(administradoresManager.getAppointment(carne),
                                JsonRequestBehavior.AllowGet);
            }

            return Json(new { Error = true, Message = "Operación HTTP desconocida" });
        }

        public JsonResult configCita(string carn, Cita item)
        {
            switch (Request.HttpMethod)
            {
                case "POST":
                    return Json(administradoresManager.InsertarCita(item));
                /*case "PUT":
                    return Json(administradoresManager.ActualizarCliente(item));*/
                case "GET":
                    return Json(administradoresManager.validarPreCitaUsuario(carn),
                                JsonRequestBehavior.AllowGet);
                /*   case "DELETE":
                      return Json(administradoresManager.EliminarCliente(id.GetValueOrDefault()));*/
            }

            return Json(new { Error = true, Message = "Operación HTTP desconocida" });
        }


        public JsonResult consultarPTratamiento(string carne, PTratamiento item)
        {
            switch (Request.HttpMethod)
            {
                case "GET":
                    return Json(administradoresManager.consultPlanesTratamiento(carne),JsonRequestBehavior.AllowGet);
                /*case "PUT":
                    return Json(administradoresManager.ActualizarCliente(item));
                case "GET":
                    return Json(administradoresManager.validarPreCitaUsuario(carn),JsonRequestBehavior.AllowGet);*/
                /*   case "DELETE":
                      return Json(administradoresManager.EliminarCliente(id.GetValueOrDefault()));*/
            }

            return Json(new { Error = true, Message = "Operación HTTP desconocida" });
        }
        public JsonResult insertarPTratamiento(PlanTratamiento item)
        {
            switch (Request.HttpMethod)
            {
                case "POST":
                    return Json(administradoresManager.insertarPlanTratamiento(item));

            }

            return Json(new { Error = true, Message = "Operación HTTP desconocida" });
        }
        public JsonResult consultarTratamientos(Tratamiento item)
        {
            switch (Request.HttpMethod)
            {
                case "GET":
                    return Json(administradoresManager.consultTratamientos(), JsonRequestBehavior.AllowGet);
                /*case "PUT":
                    return Json(administradoresManager.ActualizarCliente(item));
                case "GET":
                    return Json(administradoresManager.validarPreCitaUsuario(carn),JsonRequestBehavior.AllowGet);*/
                /*   case "DELETE":
                      return Json(administradoresManager.EliminarCliente(id.GetValueOrDefault()));*/
            }

            return Json(new { Error = true, Message = "Operación HTTP desconocida" });
        }

        public JsonResult exClinico(ExamenClinico item)
        {
            switch (Request.HttpMethod)
            {
                case "POST":
                    return Json(administradoresManager.InsertarExamenClinico(item));
                /*case "PUT":
                    return Json(administradoresManager.ActualizarCliente(item));*/

                /*   case "DELETE":
                      return Json(administradoresManager.EliminarCliente(id.GetValueOrDefault()));*/
            }

            return Json(new { Error = true, Message = "Operación HTTP desconocida" });
        }
        public JsonResult exClinico2(regDiente item)
        {
            switch (Request.HttpMethod)
            {

                case "POST":
                    return Json(administradoresManager.ActualizarDienteFromExClinico(item));
                /*   case "DELETE":
                      return Json(administradoresManager.EliminarCliente(id.GetValueOrDefault()));*/
            }

            return Json(new { Error = true, Message = "Operación HTTP desconocida" });
        }
        public JsonResult opDientes(string carne,string tipo,Diente item)
        {
            switch (Request.HttpMethod)
            {
                case "POST":
                    return Json(administradoresManager.InsertarDienteToExClinico(item));
                /*case "PUT":
                    return Json(administradoresManager.ActualizarCliente(item));*/
                case "GET":
                    return Json(administradoresManager.ObtenerExCliFromEst(carne,tipo),
                                JsonRequestBehavior.AllowGet);
                /*   case "DELETE":
                      return Json(administradoresManager.EliminarCliente(id.GetValueOrDefault()));*/
            }

            return Json(new { Error = true, Message = "Operación HTTP desconocida" });
        }
        public JsonResult opDientesUp(string carne,Diente item)
        {
            switch (Request.HttpMethod)
            {
                case "POST":
                    return Json(administradoresManager.ActualizarDiente(item));
                /*case "PUT":
                    return Json(administradoresManager.ActualizarCliente(item));*/
                case "GET":
                       return Json(administradoresManager.searchExamenClinico(carne),
                                JsonRequestBehavior.AllowGet);
                /*   case "DELETE":
                      return Json(administradoresManager.EliminarCliente(id.GetValueOrDefault()));*/
            }

            return Json(new { Error = true, Message = "Operación HTTP desconocida" });
        }
    }

    }

