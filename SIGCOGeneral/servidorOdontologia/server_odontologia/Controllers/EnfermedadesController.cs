﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using server_odontologia.Models;

namespace server_odontologia.Controllers
{
    public class EnfermedadesController : Controller
    {
        private EnfermedadManager enfermedadesManager;

        public EnfermedadesController()
        {
            enfermedadesManager = new EnfermedadManager();
        }

        // GET /Api/Clientes
        [HttpGet]
        public JsonResult Enfermedades()
        {
            return Json(enfermedadesManager.ObtenerEnfermedades(), 
                        JsonRequestBehavior.AllowGet);
        }

        // POST    Clientes/Cliente    { Nombre:"nombre", Telefono:123456789 }
        // PUT     Clientes/Cliente/3  { Id:3, Nombre:"nombre", Telefono:123456789 }
        // GET     Clientes/Cliente/3
        // DELETE  Clientes/Cliente/3
        public JsonResult Enfermedad(int? id, enfermedad item)
        {
            switch (Request.HttpMethod)
            {
                case "POST":
                    return Json(enfermedadesManager.InsertarEnfermedad(item));
                case "PUT":
                    return Json(enfermedadesManager.ActualizarCliente(item));
                case "GET":
                    return Json(enfermedadesManager.eliminarEnfermedad(id.GetValueOrDefault()), 
                                JsonRequestBehavior.AllowGet);
                case "DELETE":
                    return Json(enfermedadesManager.EliminarCliente(id.GetValueOrDefault()));
            }

            return Json(new { Error = true, Message = "Operación HTTP desconocida" });
        }
        public JsonResult Enfermedad2(int? id, enfermedad item)
        {
            switch (Request.HttpMethod)
            {
                case "POST":
                    return Json(enfermedadesManager.actualizarEnfermedad(item));
                case "PUT":
                    return Json(enfermedadesManager.ActualizarCliente(item));
                case "GET":
                    return Json(enfermedadesManager.eliminarEnfermedad(id.GetValueOrDefault()),
                                JsonRequestBehavior.AllowGet);
                case "DELETE":
                    return Json(enfermedadesManager.EliminarCliente(id.GetValueOrDefault()));
            }

            return Json(new { Error = true, Message = "Operación HTTP desconocida" });
        }
    }

    }

