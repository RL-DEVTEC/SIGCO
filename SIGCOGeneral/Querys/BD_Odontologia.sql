
create database odontologia
go
use odontologia


go
--

 CREATE TABLE Fecha(
	idFecha int identity(1,1) PRIMARY KEY,
	fecha date not null,
	UNIQUE(fecha)
 )

 GO


CREATE TABLE TRATAMIENTOS(
	ID_TRAT INT IDENTITY(1,1) PRIMARY KEY,
	TRATAMIENTO VARCHAR(100)

)

go

CREATE TABLE Persona(

	cedula			VARCHAR(11) NOT NULL,
	nombre			VARCHAR(50) NOT NULL,
	direccion		VARCHAR(100) NULL,
	ape1			VARCHAR(15) NOT NULL,
	ape2			VARCHAR(15) NOT NULL

	CONSTRAINT PK_Persona PRIMARY KEY(cedula),
	CONSTRAINT CK_Cedula CHECK ( cedula like '[0-9]-[0-9][0-9][0-9][0-9]-[0-9][0-9][0-9][0-9]') 


);

go

 CREATE TABLE Hora(
	idHora INT IDENTITY(1,1) PRIMARY KEY,
	idFecha INT NOT NULL,
	horaInicial TIME NOT NULL,
	horaFinal   TIME NOT NULL,

	CONSTRAINT FK_Hora FOREIGN KEY (idFecha) REFERENCES Fecha(idFecha) ON DELETE CASCADE ON UPDATE CASCADE
 )

go

CREATE TABLE Estudiante(
	carne           VARCHAR(10)   NOT NULL,
	pin             INT           NOT NULL,
	cedula          VARCHAR(11)   NOT NULL,
	carrera         VARCHAR(50)   NOT NULL,
	becado          INT           NOT NULL,
	sexo            CHAR          NOT NULL,
	residente       INT           NOT NULL,
	edad            INT           NOT NULL,
	estadoCivil     VARCHAR(25)   NOT NULL,
	fechaNacimiento DATE          NOT NULL,
	carneCCSS		VARCHAR(30)	  NOT NULL,

	CONSTRAINT PK_Estudiante PRIMARY KEY(carne),
	CONSTRAINT FK_CedulaEs   FOREIGN KEY (cedula) REFERENCES Persona(cedula) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT CK_Becado CHECK (becado= 0 or becado= 1),
    CONSTRAINT CK_Sexo CHECK (sexo = 'F' or sexo = 'M'),
	CONSTRAINT CK_Residente CHECK (residente = 0 or residente = 1)

);

go

CREATE TABLE Administrador(

	contrasenna   VARCHAR(8)  NOT NULL,
	tipo          VARCHAR (20)    NOT NULL,
	cedula        VARCHAR(11) NOT NULL,

	CONSTRAINT PK_Administrador PRIMARY KEY(contrasenna),
	CONSTRAINT FK_CedulaAd   FOREIGN KEY (cedula) REFERENCES Persona(cedula) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT CK_Tipo CHECK (tipo = 'administrador' or tipo = 'usuario'),
);

go

CREATE TABLE Email(

	email       VARCHAR(25)        NOT NULL,
	cedula      VARCHAR(11)    NOT NULL,

	CONSTRAINT PK_email PRIMARY KEY (email,cedula),
	CONSTRAINT FK_cedulaT FOREIGN KEY (cedula) REFERENCES Persona(cedula) ON DELETE CASCADE ON UPDATE CASCADE
);

go


CREATE TABLE Telefono(

	telefono    VARCHAR(15)        NOT NULL,
	cedula      VARCHAR(11)    NOT NULL,

	CONSTRAINT PK_telefono PRIMARY KEY (telefono,cedula),
	CONSTRAINT FK_cedulaE FOREIGN KEY (cedula) REFERENCES Persona(cedula) ON DELETE CASCADE ON UPDATE CASCADE
);

go

CREATE TABLE Enfermedad(

	idEnfermedad INT IDENTITY(1,1) NOT NULL, 
	enfermedad   VARCHAR(50)       NOT NULL,
	descripcion  VARCHAR(3000)     NULL,
	tratamiento  VARCHAR(3000)           NULL,
	UNIQUE(enfermedad),

	CONSTRAINT PK_Enfermedad PRIMARY KEY (idEnfermedad)
);

go


CREATE TABLE ExamenClinico(

	idExamen    INT IDENTITY(1,1) PRIMARY KEY,
	carne       VARCHAR(10)       NOT NULL,
	tipo        INT				  NOT NULL,
    fecha         DATETIME DEFAULT(GETDATE()) ,
	descripcion  VARCHAR(3000) DEFAULT('Sin descripcion'),

	CONSTRAINT FK_carneEstudianteExameGn FOREIGN KEY (carne) REFERENCES Estudiante(carne) ON DELETE CASCADE ON UPDATE CASCADE
);


 go

CREATE TABLE Diente(
	idTabDiente INT IDENTITY(1,1)  PRIMARY KEY,
	idExamen    INT ,
	idDiente    INT NOT NULL,
	extraccion  INT NOT NULL,
	ausente		INT NOT NULL,
	zona1		INT NOT NULL,
	zona2		INT NOT NULL,
	zona3		INT NOT NULL,
	zona4		INT NOT NULL,
	zona5		INT NOT NULL

	CONSTRAINT FK_IDExamen FOREIGN KEY (idExamen) REFERENCES ExamenClinico (idExamen)
)


go


CREATE TABLE Cita( --- revisar, esta tabla es innecesaria

	idCita       INT IDENTITY(1,1)  NOT NULL,
	fecha        DATE               NOT NULL,
	carne        VARCHAR(10)            NOT NULL,
	horaEntrada  VARCHAR(10)             NOT NULL,
	horaSalida   VARCHAR(10)           NOT NULL,

	CONSTRAINT PK_idCita PRIMARY KEY (idCita),
	CONSTRAINT FK_carneEstudianteCita FOREIGN KEY (carne) REFERENCES Estudiante(carne) ON DELETE CASCADE ON UPDATE CASCADE
	
);


go


CREATE TABLE EnfermedadEstudiante(

	idEnfermedad   INT  NOT NULL,
	carne          VARCHAR(10) NOT NULL,

	CONSTRAINT PK_EnfermedadEstudiante PRIMARY KEY (carne,idEnfermedad),
	CONSTRAINT FK_carneEs  FOREIGN KEY (carne) REFERENCES Estudiante(carne) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT FK_idEnfermedadE  FOREIGN KEY (idEnfermedad) REFERENCES Enfermedad(idEnfermedad) ON DELETE CASCADE ON UPDATE CASCADE
);

go


 CREATE TABLE FechaCita(
	idFC	INT IDENTITY(1,1) PRIMARY KEY,
	idFecha int not null,
	idHora INT NOT NULL,
	timeSaved DATETIME DEFAULT(GETDATE())
	--timeSaved TIME DEFAULT(CONVERT(TIME,(select convert(varchar(10), GETDATE(), 108)))),
	CONSTRAINT FK_idFecha FOREIGN KEY (idFecha) REFERENCES Fecha(idFecha)ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT FK_idHora_FC FOREIGN KEY (idHora) REFERENCES Hora(idHora)
 )

 go

 CREATE TABLE CitaBloq
 (
	idCB INT IDENTITY(1,1) PRIMARY KEY,
	idFC INT NOT NULL,
	descrip VARCHAR(200),

	CONSTRAINT FK_FECHA_CITA FOREIGN KEY (idFC) REFERENCES FechaCita(idFC)
 )

 GO

 CREATE TABLE CitaEst(

 	idCE INT IDENTITY(1,1) PRIMARY KEY,
	idFC INT NOT NULL,
	carne VARCHAR(10) NOT NULL,
	estado VARCHAR(15) NOT NULL,
	CONSTRAINT FK_FECHA_CITA2 FOREIGN KEY (idFC) REFERENCES FechaCita(idFC),
	CONSTRAINT FK_CARNE_ESTUDIANTE FOREIGN KEY (carne) REFERENCES Estudiante(carne)

 )


go

CREATE TABLE PlanTratamiento(
	idPlanT			INT IDENTITY(1,1) PRIMARY KEY,
	carne			VARCHAR(10)   NOT NULL,
	fecha			DATETIME DEFAULT(GETDATE()) NOT NULL,
	IdTrat	INT NOT NULL,
	diente			INT NOT  NULL,
	seccion			INT NOT NULL,
	descripcion		VARCHAR(100)  
	CONSTRAINT FK_carneEst FOREIGN KEY (carne) REFERENCES Estudiante(carne),
	CONSTRAINT FK_IdTrat   FOREIGN KEY (IdTrat) REFERENCES TRATAMIENTOS (ID_TRAT)

)





GO

/************************************************************* 
  VARIABLE TIPO TABLA PARA TRABAJAR EN PROCESOS ALMACENADOS
**************************************************************/
CREATE  TYPE RESULT AS TABLE
(
	
	FECHA_RES DATE


);

go

CREATE  TYPE TYPE_CITAS AS TABLE
(
	ID INT IDENTITY(1,1) PRIMARY KEY,
	FECHA DATE,
	IDFC INT,
	CARNE VARCHAR(30),
	ESTADO VARCHAR(30),
	NOMBRE_COMP VARCHAR(200),
	HORA_I TIME,
	HORA_F TIME,
	DIA VARCHAR(15),
	MES VARCHAR(15)

);

go

CREATE TYPE TYPE_CITAS_AUX AS TABLE
(
	FECHA DATE,
	IDFC INT,
	CARNE VARCHAR(30),
	NOMBRE_COMP VARCHAR(30),
	HORA_I TIME,
	HORA_F TIME

);

