
use odontologia
go

-- ESPACIO PARA PROCESOS ALMACENADOS
CREATE PROCEDURE insertGetExClinico
	@carne VARCHAR(10),
	@Tipo  INT
	AS
			BEGIN TRY  
				INSERT INTO ExamenClinico(carne,tipo) VALUES (@carne,@tipo);
				SELECT  CAST((SELECT TOP 1 idExamen FROM ExamenClinico ORDER BY idExamen DESC) AS varchar(10)) AS 'Id';
			END TRY  
			BEGIN CATCH  
				SELECT 'FatalError';
			END CATCH;



go


CREATE PROCEDURE ACTUALIZAR_DIENTE_EX_CLINICO_SEGUNDA_VEZ
	@CARNE		VARCHAR(10),
	@ID_DIENTE  INT,
	@EXTRACCION INT,
	@AUSENTE    INT,
	@Z1			INT,
	@Z2			INT,
	@Z3			INT,
	@Z4			INT,
	@Z5			INT
AS
	BEGIN
		DECLARE 
			@ID_EXAMEN INT
		SET @ID_EXAMEN= (SELECT idExamen from ExamenClinico WHERE carne=@CARNE AND tipo=1)


		UPDATE Diente SET extraccion=@EXTRACCION, ausente=@AUSENTE, zona1=@Z1,zona2=@Z2,zona3=@Z3,zona4=@Z4,zona5=@Z5 WHERE idExamen= @ID_EXAMEN AND idDiente= @ID_DIENTE 
	END



go

CREATE PROCEDURE actualizaAdm
	@cedPast	VARCHAR(15),
	@ced		VARCHAR(15),
	@nom		VARCHAR(40),
	@direc		VARCHAR(40),
	@ape1		VARCHAR(15),
	@ape2		VARCHAR(15),
	@email		VARCHAR(20),
	@telefono	VARCHAR(20),
	@contra		VARCHAR(20),
	@tipo		VARCHAR(15)
AS
	
	UPDATE Email SET email=@email WHERE cedula=@cedPast ;
	UPDATE Telefono SET telefono=@telefono WHERE cedula=@cedPast ;
	UPDATE Administrador SET contrasenna=@contra, tipo=@tipo WHERE cedula=@cedPast ;
	UPDATE Persona SET cedula=@ced, nombre = @nom , direccion=@direc, ape1=@ape1, ape2=@ape2 WHERE cedula=@cedPast ;


go


/***************************************************************************************
  PROCESO ALMACENADO PARA CONSULTAR PLANES DE TRATAMIENTO
****************************************************************************************/
--exec CONSULTAR_PTRATAMIENTO_EST 201500777  select*from PlanTratamiento
CREATE  PROCEDURE CONSULTAR_PTRATAMIENTO_EST
	@CARNE VARCHAR(30)
AS
	BEGIN-- select*from  PlanTratamiento
		SELECT pl.fecha,tra.TRATAMIENTO,pl.diente, pl.idPlanT,pl.descripcion from PlanTratamiento as pl 
		INNER JOIN TRATAMIENTOS AS tra on(tra.ID_TRAT=pl.IdTrat) WHERE carne=@CARNE
	END

go
/**** select*from PlanTratamiento***********************************************************************************
  PROCESO ALMACENADO PARA INSERTAR PLAN DE TRATAMIENTO delete  delete from diente; delete  from ExamenClinico;delete select* from PlanTratamiento
****************************************************************************************/

CREATE PROCEDURE INSERTAR_PLAN_TRATAMIENTO   --SELECT*FROM  Enfermedad
	@CARNE VARCHAR(30),
	@TRATAMIENTO VARCHAR(40),
	@DIENTE INT,
	@SECCION INT,
	@DESCRIPCION VARCHAR(100)
AS
	DECLARE
		@ID_TRAT INT
	BEGIN
		SET @ID_TRAT= (SELECT ID_TRAT FROM TRATAMIENTOS WHERE TRATAMIENTO=@TRATAMIENTO)
		INSERT INTO PlanTratamiento(carne,fecha,IdTrat,diente,seccion,descripcion) values (@CARNE,GETDATE(),@ID_TRAT,@DIENTE,@SECCION,@DESCRIPCION)
	END




go

/***************************************************************************************
  PROCESO ALMACENADO PARA CONSULTAR  TRATAMIENTOS
****************************************************************************************/

CREATE PROCEDURE OBTENER_TRATAMIENTOS
AS
	BEGIN
		SELECT TRATAMIENTO FROM TRATAMIENTOS
	END


go

/***********************************************
  PROCESO ALMACENADO ELIMINAR CITA
************************************************/ --select*from fechacita update fecha set fecha = '2018-02-16' where idFecha = 7

CREATE PROCEDURE DEL_CITA_FROM_ID  --SELECT*FROM HORA
	@ID_CITA INT
AS
	DECLARE 
		@AUX_IDFECHA INT,
		@AUX_IDHORA  INT

	BEGIN

		SET @AUX_IDFECHA = (SELECT idFecha FROM FechaCita WHERE idFC = @ID_CITA)
		SET @AUX_IDHORA = (SELECT idHora FROM FechaCita WHERE idFC = @ID_CITA)

		
		DELETE FROM CitaEst WHERE idFC=@ID_CITA
		DELETE FROM CitaBloq WHERE idFC=@ID_CITA
		DELETE FROM FechaCita WHERE idFC = @ID_CITA
		DELETE FROM HORA WHERE idHora = @AUX_IDHORA AND idFecha = @AUX_IDFECHA
	END
GO



----- Consulta que determina si un estudiante tiene o no alguna cita pendiente, y retorna un 1 o 0 seg�n corresponda 

 CREATE PROCEDURE obtenerPermiso
	@carne VARCHAR(30)
	AS	
		IF((SELECT CE.carne FROM FechaCita AS FC INNER JOIN CitaEst AS CE ON (CE.idFC=FC.idFC AND CE.estado = 'Pendiente')) IS NOT NULL)
			BEGIN
				Select CAST(1 AS VARCHAR(10)) as 'num';--Indica que el usuario tiene citas pendientes
			END			
		ELSE
			BEGIN
				Select CAST(2 AS VARCHAR(10)) as 'num';--Indica que el usuario no tiene citas pendientes
			END
						
go



/*****************************************************************************************
  PROCESO ALMACENADO CONSULTAR LA INFORMACI�N DE UNA CITA 
******************************************************************************************/
CREATE  PROCEDURE GET_DATE -- SELECT * FROM FECHACITA
@ID INT
AS
	BEGIN
		SELECT CE.carne, (per.nombre+' '+per.ape1+' '+per.ape2) AS 'Nombre',fe.fecha, hor.horaInicial, hor.horaFinal FROM FechaCita AS fech 
		INNER JOIN CitaEst as CE on (fech.idFC = CE.idFC)
		INNER JOIN Estudiante AS est ON (est.carne=CE.carne)
		INNER JOIN Persona AS per ON (per.cedula=est.cedula)
		INNER JOIN Fecha AS fe ON (fe.idFecha=fech.idFecha)
		INNER JOIN Hora AS hor ON (hor.idHora = fech.idHora)
		WHERE fech.idFC=@ID --select*from fechacita

		

		UNION 

		SELECT (SELECT 'NT') AS 'carne', (CE.descrip) AS 'Nombre',fe.fecha, hor.horaInicial, hor.horaFinal FROM FechaCita AS fech 
		INNER JOIN CitaBloq as CE on (fech.idFC = CE.idFC)
		INNER JOIN Fecha AS fe ON (fe.idFecha=fech.idFecha)
		INNER JOIN Hora AS hor ON (hor.idHora = fech.idHora)
		WHERE fech.idFC=@ID --select*from fechacita

		

	END

GO


/*****************************************************************************************
  PROCESO ALMACENADO PARA RECUPERAR TABLA CON INFORMACI�N DE CITAS DE UNA FECHA ESPEC�FICA
******************************************************************************************/
--dbo.OBTENER_CITAS_FECHA '2018-04-06',1
-- SELECT*FROM CitaEst
--select*from CitaBloq
CREATE PROCEDURE OBTENER_CITAS_FECHA
@FECHA_FIRST VARCHAR(20),
@TIPO INT
AS
	DECLARE
		@RESULT_X AS RESULT,
		@CITAS AS TYPE_CITAS,
		@AUX_DATE  DATE,
		@TYPE_CITAS_AUX DATE,
		@ID_FECHA INT,
		@ID_Fecha_final INT,
		@Fecha DATE,
		@carne varchar(30),
		@estado VARCHAR(30),
		@HI TIME,
		@HF TIME,
		@Nombre varchar(40),

		-- VARS TO GENERATE DATES 
		@FECHA_WORK_W DATE = CAST((@FECHA_FIRST)AS DATE),
		@FECHA_AUX_W DATE,
		@DAY VARCHAR(10),
		@SUMA INT=0,
		@RESTA INT=0,
		@RESULT_X_W AS RESULT,
		@PRE_RESA INT,
		@PRE_RESB VARCHAR(25),
		@PRE_RESC VARCHAR(25),
		@AUX_WORK VARCHAR(25)


		-----
		
	BEGIN

		IF(@TIPO=0) -- SE DESEA SABER LAS CITAS PARA UNA FECHA ESPEC�FICA
			BEGIN

				INSERT INTO @RESULT_X_W(FECHA_RES) VALUES (@FECHA_FIRST)

			END
		ELSE -- 1 PARA LAS CITAS DE TODA UNA SEMANA, DE DIAS ALREDEDOR DE UNA FECHA ESPEC�FICA
			BEGIN

------------------------------------------------------------------------------

				SET @DAY = (SELECT DATENAME(dw,@FECHA_FIRST))  -- (SELECT DATENAME(dw,GETDATE()))
				IF(@DAY='Monday')
					BEGIN
						SET @SUMA=4				
					END
				ELSE IF(@DAY='Tuesday')
					BEGIN
						SET @SUMA=3
						SET @RESTA=1
					END
				ELSE IF(@DAY='Wednesday')
					BEGIN
						SET @SUMA=2
						SET @RESTA=2
					END
				ELSE IF(@DAY='Thursday')
					BEGIN
						SET @SUMA=1
						SET @RESTA=3
					END
				ELSE
					BEGIN
						SET @RESTA=4
					END
				INSERT INTO @RESULT_X_W(FECHA_RES) VALUES (@FECHA_FIRST)-----AQUI EST� EL ERROR
				--PRINT 'A'
				SET @FECHA_AUX_W = @FECHA_WORK_W
				WHILE(@RESTA<>0)
					BEGIN
						SET @FECHA_AUX_W = (SELECT DATEADD(day, -1, @FECHA_AUX_W) AS DateAdd);
						SET @RESTA-=1
						INSERT INTO @RESULT_X_W(FECHA_RES) VALUES (@FECHA_AUX_W)
						PRINT 'B'

					END
				SET @FECHA_AUX_W = @FECHA_WORK_W
				WHILE(@SUMA<>0)
					BEGIN
						SET @FECHA_AUX_W = (SELECT DATEADD(day, 1, @FECHA_AUX_W) AS DateAdd);
						SET @SUMA-=1
						INSERT INTO @RESULT_X_W(FECHA_RES) VALUES (@FECHA_AUX_W)
						PRINT 'C'

					END
		END		
		DECLARE CURSOR_CITAS CURSOR FOR
		SELECT FECHA_RES FROM @RESULT_X_W 

		OPEN CURSOR_CITAS
		FETCH NEXT FROM CURSOR_CITAS
		INTO @AUX_DATE

		WHILE @@FETCH_STATUS = 0 -- sel
		BEGIN
		--select*from FECHAcita
			SET @ID_FECHA = ( SELECT idFecha FROM FECHA  WHERE fecha = @AUX_DATE)
			DECLARE CURSOR_FECHAS CURSOR FOR

			SELECT Fecha, ID_Fecha,carne,estado, HI, HF, Nombre FROM (SELECT @AUX_DATE as 'Fecha',FC.idFC  as 'ID_Fecha', CE.carne as 'carne',CE.estado as 'estado', ora.horaInicial as 'HI', ora.horaFinal as 'HF', per.nombre+' '+per.ape1+' '+per.ape2 as 'Nombre'   FROM FechaCita as FC 
			INNER JOIN CitaEst as CE ON (CE.idFC=FC.idFC)
			INNER JOIN Hora as ora on (FC.idHora=ora.idHora) 
			INNER JOIN Estudiante AS est on (est.carne = CE.carne) 
			INNER JOIN Persona as per on (per.cedula=est.cedula) 
			WHERE FC.idFecha=@ID_FECHA ) as t1

			UNION 

			SELECT Fecha, ID_Fecha,carne,estado, HI, HF, Nombre FROM (SELECT @AUX_DATE as 'Fecha',FC.idFC  as 'ID_Fecha', (SELECT 'NT') as 'carne',(SELECT 'inhabilitada') as 'estado', ora.horaInicial as 'HI', ora.horaFinal as 'HF', CE.descrip as 'Nombre'   FROM FechaCita as FC 
			INNER JOIN CitaBloq as CE ON (CE.idFC=FC.idFC)
			INNER JOIN Hora as ora on (FC.idHora=ora.idHora)  
			WHERE FC.idFecha=@ID_FECHA ) as t2


			OPEN CURSOR_FECHAS
			FETCH NEXT FROM CURSOR_FECHAS
			INTO @Fecha, @ID_Fecha_final, @carne,@estado, @HI, @HF, @Nombre
			WHILE @@FETCH_STATUS = 0 
				BEGIN
				
				SET @AUX_WORK = (SELECT SUBSTRING(CONVERT(VARCHAR(25),@HI),1,2))

				IF((CONVERT(INT,@AUX_WORK)) >= 1 AND (CONVERT(INT,@AUX_WORK)) <= 5)
					BEGIN
						SET @PRE_RESB = (SELECT CONVERT(VARCHAR(10),FORMAT(CONVERT(TIME,(SELECT @AUX_WORK +':'+ SUBSTRING(CONVERT(VARCHAR(25),@HI),4,2)+'PM' )),'hhmm','en-us')))

						SET @PRE_RESB = (SELECT SUBSTRING(@PRE_RESB,1,2)+':'+ SUBSTRING(@PRE_RESB,3,2)) -- SELECT SUBSTRING(SELECT CONVERT(VARCHAR(25),CONVERT(TIME,'09:10'))  ,)
					
						SET @HI = (SELECT CONVERT(TIME,@PRE_RESB))
					END

				SET @AUX_WORK = (SELECT SUBSTRING(CONVERT(VARCHAR(25),@HF),1,2))

				IF((CONVERT(INT,@AUX_WORK)) >= 1 AND (CONVERT(INT,@AUX_WORK)) <= 5)
					BEGIN
						SET @PRE_RESB = (SELECT CONVERT(VARCHAR(10),FORMAT(CONVERT(TIME,(SELECT @AUX_WORK +':'+ SUBSTRING(CONVERT(VARCHAR(25),@HF),4,2)+'PM' )),'hhmm','en-us')))

						SET @PRE_RESB = (SELECT SUBSTRING(@PRE_RESB,1,2)+':'+ SUBSTRING(@PRE_RESB,3,2)) -- SELECT SUBSTRING(SELECT CONVERT(VARCHAR(25),CONVERT(TIME,'09:10'))  ,)
					
						SET @HF = (SELECT CONVERT(TIME,@PRE_RESB))
					END

					INSERT INTO @CITAS(FECHA,IDFC,CARNE,ESTADO,NOMBRE_COMP,HORA_I,HORA_F,DIA,MES) VALUES(@Fecha,@ID_Fecha_final,@carne,@estado,@Nombre,@HI,@HF,(SELECT DATENAME(dw,@Fecha)),(SELECT DATENAME(m,@Fecha)));
					
					FETCH NEXT FROM CURSOR_FECHAS
					INTO @Fecha, @ID_Fecha_final, @carne,@estado, @HI, @HF, @Nombre
				END
			CLOSE CURSOR_FECHAS
			DEALLOCATE CURSOR_FECHAS					
			FETCH NEXT FROM CURSOR_CITAS
			INTO @AUX_DATE
		END
		CLOSE CURSOR_CITAS
		DEALLOCATE CURSOR_CITAS
		SELECT ID,FECHA,IDFC,CARNE,NOMBRE_COMP,HORA_I,HORA_F,DIA,MES,ESTADO FROM @CITAS;
	END

GO

/*********************************************************************************************************************************************
PROCESO ALMACENADO PARA CALCULAR ID_FECHA EN LA TABLA FECHA POR SI SE TRATA DE SACAR UNA CITA EL MISMO DIA O UNO QUE A�N NO HA SIDO REGISTRADO
**********************************************************************************************************************************************/


CREATE PROCEDURE CALC_ID_FECHA
@FECHA VARCHAR(20)
AS
	DECLARE
		@FECHA_DATE DATE = (CONVERT(DATE,@FECHA)) -- SELECT*FROM FECHA
	BEGIN
		IF((SELECT idFecha FROM Fecha WHERE fecha = @FECHA_DATE) IS NULL) --CITA NUEVA PARA UN NUEVO DIA
			BEGIN
				INSERT INTO Fecha(fecha) VALUES (@FECHA_DATE)
				RETURN (SELECT idFecha from Fecha WHERE fecha = @FECHA_DATE)
			END
		ELSE-- SE NECESITA EL idFecha DE UNA FECHA YA CREADA
			BEGIN
				RETURN (SELECT idFecha from Fecha WHERE fecha = @FECHA_DATE)
			END
		
	END

GO
/*****************************************************************************CONVERT(DATE,@FECHA_INICIAL) select*from fechacita
  PROCESO ALMACENADO PARA CONSULTAR  O REGISTRAR UNAS NUEVA CITA EN EL SISTEMA // 1->
******************************************************************************/

--DBO.CONSULTAR_CREAR_CITA  '201500777','2018-02-10','8:45','9:45'
/*
DELETE FROM FECHACITA
DELETE FROM Hora
DELETE FROM Fecha

SELECT * FROM citaest FECHACITA
SELECT * FROM citaBloq
SELECT * FROM Fecha
 ---  dbo.consultar_crear_cita '201500777','2018-03-01','13:00:00','14:00:00'  select*from fechacita
*/--carne=201500777&fecha=2018-3-2&horaI=03:00&horaF=04:00&tipo=2&preID=2
-- dbo.CONSULTAR_CREAR_CITA '201500777','2018-3-21','09:00','10:00',1,1
-- select * from CitaEst
-- select*from fechaCita
-- select * from CitaBloq
-- select*from 

--dbo.CONSULTAR_CREAR_CITA  'no','2018-03-21','08:00','08:45',1,1

--select*from fechacita citaBloq  


--  select timeSaved from (select top 1 * from fechaCita order by timeSaved DESC) as t1


/*SELECT total_seconds = DATEPART(SECOND, (SELECT CONVERT(VARCHAR(8),GETDATE(),108))) + 60 * DATEPART(MINUTE,(SELECT CONVERT(VARCHAR(8),GETDATE(),108))) + 3600 * DATEPART(HOUR, (SELECT CONVERT(VARCHAR(8),GETDATE(),108)))

	SELECT total_seconds = DATEPART(SECOND, (SELECT CONVERT(VARCHAR(8),(CONVERT(DATETIME,@HORA_A)),108))) + 60 * DATEPART(MINUTE,(SELECT CONVERT(VARCHAR(8),(CONVERT(DATETIME,@HORA_A)),108))) + 3600 * DATEPART(HOUR, (SELECT CONVERT(VARCHAR(8),(CONVERT(DATETIME,@HORA_A)),108)))

	SELECT total_seconds =
    DATEPART(SECOND, (SELECT CONVERT(VARCHAR(8),(CONVERT(DATETIME,'8:10:01:02')),108))) +
    60 * DATEPART(MINUTE,(SELECT CONVERT(VARCHAR(8),(CONVERT(DATETIME,'8:10:01:02')),108))) +
    3600 * DATEPART(HOUR, (SELECT CONVERT(VARCHAR(8),(CONVERT(DATETIME,'8:10:01:02')),108)))

	SELECT total_seconds =
    DATEPART(SECOND, (SELECT CONVERT(VARCHAR(8),(CONVERT(DATETIME,'8:10:01:03')),108))) +
    60 * DATEPART(MINUTE,(SELECT CONVERT(VARCHAR(8),(CONVERT(DATETIME,'8:10:01:03')),108))) +
    3600 * DATEPART(HOUR, (SELECT CONVERT(VARCHAR(8),(CONVERT(DATETIME,'8:10:01:03')),108)))


	holar*/

--(SELECT CONVERT(TIME,(SELECT CONVERT(VARCHAR(8),(select timeSaved from (select top 1 * from fechaCita order by timeSaved DESC) as t1),108))))

/*
CREATE TABLE PRUEBA(    -- INSERT INTO PRUEBA(DESCR) VALUES ()
	DESCR VARCHAR(100)

)



*/
 -- select*  from prueba

CREATE TABLE PRUEBA(    -- INSERT INTO PRUEBA(DESCR) VALUES ()   select * DELETE from prueba   SELECT * FROM FECHACita
	DESCR VARCHAR(100)

)
GO
CREATE PROCEDURE CONSULTAR_CREAR_CITA   -- dbo.CONSULTAR_CREAR_CITA 'no','2018-03-21','10:10:00','11:10:00',3,1,'d'
@CARNE VARCHAR(20),
@FECHA VARCHAR(20),
@HORAI  VARCHAR(20),
@HORAF  VARCHAR(20),
@TIPO   INT, -- 1-> INSERCI�N NORMAL // 2->INSERCI�N NORMAL CON ELIMINACI�N DE CITA INHABILITADA
@ID_CITA_INHAB INT,
@DESCR VARCHAR(100)
AS
	DECLARE
		@ID_FECHA INT,
		@FECHA_DATE DATE = (CONVERT(DATE,@FECHA)),
		@HORA_I TIME     = (CONVERT(TIME,@HORAI)),
		@HORA_F TIME     = (CONVERT(TIME,@HORAF)),
		@ID_HORA INT,
		@AUX_DATO INT,
		@AUX_DATO2 INT,
		@HORA_A DATETIME,
		@HORA_B DATETIME,
		@HORA_AN INT,
		@HORA_BN INT,
		@AUX_GENERAL INT = 0
		
	BEGIN
	
		SET @HORA_A = (select timeSaved from (select top 1 * from fechaCita order by timeSaved DESC) as t1)-- HORA Y FECHA DEL �LTIMO REGISTRO INSERTADO

		IF(@HORA_A IS NOT NULL)
			BEGIN
				SET @HORA_AN = (DATEPART(SECOND, (SELECT CONVERT(VARCHAR(8),(CONVERT(DATETIME,@HORA_A)),108))) + 60 * DATEPART(MINUTE,(SELECT CONVERT(VARCHAR(8),(CONVERT(DATETIME,@HORA_A)),108))) + 3600 * DATEPART(HOUR, (SELECT CONVERT(VARCHAR(8),(CONVERT(DATETIME,@HORA_A)),108))))
				SET @HORA_BN = (DATEPART(SECOND, (SELECT CONVERT(VARCHAR(8),GETDATE(),108))) + 60 * DATEPART(MINUTE,(SELECT CONVERT(VARCHAR(8),GETDATE(),108))) + 3600 * DATEPART(HOUR, (SELECT CONVERT(VARCHAR(8),GETDATE(),108))))

				IF((@HORA_BN - @HORA_AN) >= 1 )
					BEGIN 
						IF((SELECT CARNE FROM ESTUDIANTE WHERE CARNE = @CARNE) IS NULL AND @CARNE <> 'no')
							BEGIN
								SELECT 1; -- NO EXISTE EL ESTUDIANTE   DBO.OBTENER_CITAS_FECHA '2018-04-20', 1
							END
						ELSE
							BEGIN
								IF(@TIPO=2)
									BEGIN
										EXEC DEL_CITA_FROM_ID @ID_CITA_INHAB
										SET @AUX_GENERAL = 1;
									END
								EXECUTE @ID_FECHA = CALC_ID_FECHA @FECHA_DATE
								SET @AUX_DATO = (SELECT idHora FROM HORA WHERE idFecha=@ID_FECHA AND horaInicial=@HORA_I AND horaFinal = @HORA_F)
								IF(@AUX_DATO IS NULL)
									BEGIN
										INSERT INTO Hora(idFecha, horaInicial, horaFinal) VALUES (@ID_FECHA,@HORA_I,@HORA_F)
										SET @AUX_DATO = (SELECT idHora FROM HORA WHERE idFecha=@ID_FECHA AND horaInicial=@HORA_I AND horaFinal = @HORA_F)
									END
								SET @ID_HORA = @AUX_DATO
							
								SET @AUX_DATO2 = (SELECT idFC FROM FechaCita WHERE idFecha = @ID_FECHA AND idHora=@ID_HORA)

								--INSERT INTO PRUEBA(MSJ) VALUES ('A')   -- select * from fechaCita
								IF(@AUX_DATO2 IS NULL OR @AUX_GENERAL = 1)
									BEGIN
										INSERT INTO FechaCita(idFecha,idHora) VALUES (@ID_FECHA,@ID_HORA)--********************** SELECT * FROM FECHAcITA  select * from citaEst
										--SELECT MAX(ID_INC_EMP)+1 FROM SIG_SOC.INCAPACIDAD_EMPLEADO_OPERACION
										SET @AUX_DATO2 = (SELECT MAX(idFC) FROM FechaCita WHERE idFecha = @ID_FECHA AND idHora=@ID_HORA)

										INSERT INTO PRUEBA(DESCR) VALUES ('here A: '+CONVERT(VARCHAR(10),@AUX_DATO2))
									END			
								IF(@TIPO=3)
									BEGIN		
										INSERT INTO CitaBloq(idFC,descrip) VALUES (@AUX_DATO2,@DESCR)										
									END
								ELSE
									BEGIN
										INSERT INTO CitaEst(idFC,carne,estado) VALUES (@AUX_DATO2, @CARNE,'pendiente') -- select * from citaEst fechaCita
										INSERT INTO PRUEBA(DESCR) VALUES ('here B: ')
									END
								SELECT 2 -- REPRESENTA LA CREACI�N DE UNA CITA SATISFACTORIAMENTE SELECT * FROM FECHACITA
							END
					END

			END
		ELSE
			BEGIN

				IF((SELECT CARNE FROM ESTUDIANTE WHERE CARNE = @CARNE) IS NULL AND @CARNE <> 'no')
					BEGIN
						SELECT 1; -- NO EXISTE EL ESTUDIANTE
					END
				ELSE
					BEGIN
						IF(@TIPO=2)
							BEGIN
								EXEC DEL_CITA_FROM_ID @ID_CITA_INHAB
							END
						EXECUTE @ID_FECHA = CALC_ID_FECHA @FECHA_DATE
						SET @AUX_DATO = (SELECT idHora FROM HORA WHERE idFecha=@ID_FECHA AND horaInicial=@HORA_I AND horaFinal = @HORA_F)
						IF(@AUX_DATO IS NULL)
							BEGIN
								INSERT INTO Hora(idFecha, horaInicial, horaFinal) VALUES (@ID_FECHA,@HORA_I,@HORA_F)
								SET @AUX_DATO = (SELECT idHora FROM HORA WHERE idFecha=@ID_FECHA AND horaInicial=@HORA_I AND horaFinal = @HORA_F)
							END


						-- SELECT * FROM HORA

				
			
						SET @ID_HORA = @AUX_DATO
					
						SET @AUX_DATO2 = (SELECT idFC FROM FechaCita WHERE idFecha = @ID_FECHA AND idHora=@ID_HORA)
						--INSERT INTO PRUEBA(MSJ) VALUES ('B')
						IF(@AUX_DATO2 IS NULL)
							BEGIN
								INSERT INTO FechaCita(idFecha,idHora) VALUES (@ID_FECHA,@ID_HORA)--*****
								SET @AUX_DATO2 = (SELECT idFC FROM FechaCita WHERE idFecha = @ID_FECHA AND idHora=@ID_HORA)
							END

				

						IF(@TIPO=3)
							BEGIN
		
								INSERT INTO CitaBloq(idFC,descrip) VALUES (@AUX_DATO2,@DESCR)
								--INSERT INTO CitaEst(idFC,carne,estado) VALUES ((SELECT idFC FROM FechaCita WHERE idFecha = @ID_FECHA AND idHora = @ID_HORA), @CARNE,'pendiente')
							END
						ELSE
							BEGIN
								INSERT INTO CitaEst(idFC,carne,estado) VALUES (@AUX_DATO2, @CARNE,'pendiente')
							END

				

						-- SELECT* FROM CitaBloq




						SELECT 2 -- REPRESENTA LA CREACI�N DE UNA CITA SATISFACTORIAMENTE SELECT * FROM FECHACITA

			
					END

			END

	END


go




/***********************************************
  PROCESO ALMACENADO CAMBIAR ESTADO DE UNA CITA
************************************************/ --SELECT * FROM FECHACITA  dbo.CAMBIAR_ESTADO_CITA 35,'inhabilitada'
-- SELECT * FROM CitaEst
-- SELECT * FROM CitaBloq
CREATE PROCEDURE CAMBIAR_ESTADO_CITA   --DBO.CAMBIAR_ESTADO_CITA 1,'noshow','asds'
	@ID_CITA INT,
	@ESTADO VARCHAR(15),
	@DESCRIP VARCHAR(100)
AS
	BEGIN
		DECLARE
			@AUX VARCHAR(20)

		IF(@ESTADO = 'inhabilitada')
			BEGIN
				DELETE FROM CitaEst WHERE idFC = @ID_CITA
				--select* from CitaBloq
				IF((SELECT idCB FROM CitaBloq WHERE idFC=@ID_CITA) IS NULL)
					BEGIN -- SELECT* DELETE FROM CITABLOQ
						INSERT INTO CITABLOQ(idFC,descrip) VALUES (@ID_CITA,@DESCRIP)							
					END
			END
		ELSE
			BEGIN
				UPDATE CitaEst SET estado = @ESTADO WHERE idFC = @ID_CITA
				/*IF((SELECT idFC FROM CitaEst WHERE idFC = @ID_CITA) IS NULL)
					BEGIN-- select*from CitaEst
						SET @AUX = (SELECT carne from CitaEst WHERE idFC = @ID_CITA);
						--DELETE FROM CitaBloq WHERE idFC = @ID_CITA
						INSERT INTO CitaEst(idFC,carne,estado) VALUES(@ID_CITA,@AUX,@ESTADO)
					END
				ELSE
					BEGIN
						
					END*/
				
			END
		
	END
		
GO


/*************************************************************************************
  PROCESO ALMACENADO PARA CONSULTAR INFORMACI�N DE ESTUDIANTES DE LA VISTA ESTUDIANTES
**************************************************************************************/ --select * from Persona Estudiante
--dbo.GET_ESTUDIANTES_VIEW
CREATE PROCEDURE GET_ESTUDIANTES_VIEW
AS
	BEGIN
		SELECT per.nombre+' '+per.ape1+' '+per.ape2 AS 'Nombre',est.carne,est.cedula,est.carrera,est.sexo,est.edad,est.becado FROM Estudiante AS est
		INNER JOIN Persona AS per ON (per.cedula = est.cedula) 
	END

GO 

CREATE PROCEDURE GET_APPOINTMENT
AS
	DECLARE 
		@CARNE VARCHAR(10)
		
	BEGIN
		SELECT f.fecha, h.horaInicial FROM FechaCita as fc 
			INNER JOIN CitaEst AS CE ON (CE.idFC=fc.idFC) 
			INNER JOIN Hora as h on (h.idHora=fc.idHora) 
			INNER JOIN Fecha as f ON (f.idFecha=fc.idFecha) WHERE CE.carne=@CARNE		
	END


GO

CREATE PROCEDURE HOUR_FROM_DATE
AS
	DECLARE
		@FECHA VARCHAR(23)

	BEGIN

		Select H.horaInicial as 'Hora'  From Fecha as F 
			INNER JOIN Hora as H on(F.idFecha=H.idFecha) 
			LEFT OUTER JOIN FechaCita as FC on(FC.idHora=H.idHora and FC.idFecha=F.idFecha) 
			WHERE F.fecha=@FECHA AND FC.idHora IS NULL
	END

GO

-- dbo.OBTENER_EX_CLI_FROM_EST '201500777',0

CREATE PROCEDURE OBTENER_EX_CLI_FROM_EST
@CARNE VARCHAR(20),
@TIPO VARCHAR(20)
AS
	BEGIN
		SELECT ex.idExamen, d.idDiente,d.extraccion,d.ausente,d.zona1,d.zona2,d.zona3,d.zona4,d.zona5 
			FROM ExamenClinico as ex 
			INNER JOIN Diente as d ON (ex.idExamen= d.idExamen) 
		WHERE ex.carne=@CARNE and ex.tipo=@TIPO
	END

GO

CREATE PROCEDURE OBTENER_ADMINISTRADORES
AS
	BEGIN
		SELECT p.cedula,p.nombre,p.ape1,p.ape2,p.direccion,e.email,tel.telefono,adm.contrasenna,adm.tipo FROM Persona AS p 
			INNER JOIN   Email as e on (e.cedula=p.cedula) 
			INNER JOIN	Administrador AS adm on adm.cedula=p.cedula 
			INNER JOIN Telefono AS tel on (p.cedula=tel.cedula)
	END

GO

CREATE PROCEDURE ACTUALIZAR_DIENTE
AS
	DECLARE
		@EXTRAC		INT,
		@AUS		INT,
		@Z1			INT,
		@Z2			INT,
		@Z3			INT,
		@Z4			INT,
		@Z5			INT,
		@ID_EXAMEN	INT,
		@ID_DIENTE  INT
	BEGIN
		UPDATE Diente 
			SET extraccion=@EXTRAC, 
			ausente=@AUS,
			zona1=@Z1,
			zona2=@Z2, 
			zona3=@Z3, 
			zona4=@Z4, 
			zona5=@Z5 
		WHERE idExamen=@ID_EXAMEN and idDiente=@ID_DIENTE
	END


GO


/***********************************************
  PROCESO ALMACENADO QUE CONSULTA EL ESTADO DE UNA CITA  Y RETORNA 1 -> SI EST� INHABILITADA, 2 ASIGNADA A ESTUDIANTE
************************************************/
CREATE PROCEDURE CONSULTAR_EC   --CONSULTA EL ESTADO  dbo.CONSULTAR_EC 2
@ID_CITA INT
AS
	--DECLARE  --select*from citabloq
	BEGIN
		IF((SELECT idFC FROM CitaBloq WHERE idFC = @ID_CITA) IS NOT NULL)
			BEGIN
				SELECT 1 -- INDICA QUE EXISTE EN LA TABLA DE BLOQUEOS
			END
		ELSE
			BEGIN
				SELECT 2 -- INDICA ASOCIACI�N CON UN ESTUDIANTE
			END
	END

GO
/************************************************************************  �--select*from CitaBloq
  PROCESO ALMACENADO QUE CONSULTA LA DESCRIPCI�N DE UNA CITA INHABILITADA   DBO.CONSULTAR_DESC_CITA_INHAB 7
*************************************************************************/
CREATE PROCEDURE CONSULTAR_DESC_CITA_INHAB
@ID_CITA INT
AS
	BEGIN
		--SELECT*FROM CitaBloq
		SELECT descrip FROM CitaBloq WHERE idFC=@ID_CITA
	END

go
--select  dbo.CONSULTAR_EST_CITA_ASOC 46
/************************************************************************  �--select*from CitaEst
  PROCESO ALMACENADO QUE CONSULTAR NOMBRE COMPLETO DE ESTUDIANTE ASOC A CITA
*************************************************************************/
CREATE PROCEDURE CONSULTAR_EST_CITA_ASOC
@ID_CITA INT
AS
	BEGIN
		--SELECT*FROM CitaBloq
		SELECT PER.nombre+' '+PER.ape1+' '+PER.ape2 FROM FechaCita AS FC INNER JOIN CitaEst as CE ON (FC.idFC=CE.idFC)
		INNER JOIN Estudiante AS EST ON (EST.carne=CE.carne)
		INNER JOIN Persona AS PER ON (PER.cedula=EST.cedula)
	END


GO

/************************************************************************
  PROCESO ALMACENADO QUE CONSULTA SI E PUEDE CREAR UNA CITA O NO  // 0->NO // 1 -> SI
*************************************************************************/

-- dbo.VALIDAR_PRECITA '8:30','9:15','2018-03-19'
CREATE PROCEDURE VALIDAR_PRECITA
@HORAI VARCHAR(20),
@HORAF VARCHAR(20),
@FECHA VARCHAR(20)
AS
	DECLARE
		@DEF_HORAI TIME = (CONVERT(TIME,@HORAI)),
		@DEF_HORAF TIME = (CONVERT(TIME,@HORAF)),
		@DEF_FECHA DATE = (CONVERT(DATE,@FECHA))
	BEGIN
		IF((SELECT ho.idHora FROM fecha AS fe INNER JOIN FechaCita AS fc ON (fe.idFecha=fc.idFecha) INNER JOIN Hora AS ho ON (fc.idHora = ho.idHora AND ((@DEF_HORAI > ho.horaInicial AND @DEF_HORAI < ho.horaFinal) OR (@DEF_HORAF > ho.horaInicial AND @DEF_HORAF < ho.horaFinal) OR (@DEF_HORAI < ho.horaInicial  AND @DEF_HORAF > ho.horaFinal))) WHERE fe.fecha=@DEF_FECHA) IS NOT NULL)
			BEGIN
				SELECT 0
			END
		ELSE
			BEGIN
				SELECT 1
			END
	END

go


/************************************************************************
  PROCESO ALMACENADO QUE DEVUELVE ID_CITA DE UNA CITA RECIEN CREADA PARA ACTUALIZACI�N DE ID VISUAL DEL EVENTO
*************************************************************************/

CREATE PROCEDURE GET_DATE_INSERTED
@HORAI VARCHAR(20),
@HORAF VARCHAR(20),
@FECHA VARCHAR(20)
AS
	DECLARE
		@DEF_HORAI TIME = (CONVERT(TIME,@HORAI)),
		@DEF_HORAF TIME = (CONVERT(TIME,@HORAF)),
		@DEF_FECHA DATE = (CONVERT(DATE,@FECHA))
	BEGIN

		SELECT fc.idFC FROM Fecha AS fe INNER JOIN FechaCita AS fc ON (fc.idFecha=fe.idFecha) 
		INNER JOIN Hora AS ho ON (ho.idHora=fc.idHora AND ho.horaInicial=@DEF_HORAI AND ho.horaFinal=@DEF_HORAF) WHERE fe.fecha=@DEF_FECHA

	END

GO
/************************************************************************
  PROCESO ALMACENADO QUE GESTIONA EX�MEN CLINICO DE UN ESTUDIANTE
*************************************************************************/

CREATE PROCEDURE searchExamenClinico
@CARNE VARCHAR(20)
AS

	BEGIN
		
		IF((SELECT carne FROM Estudiante WHERE carne = @CARNE) IS NULL)
			BEGIN
				SELECT '1'
			END
		ELSE IF((SELECT COUNT(carne) FROM ExamenClinico WHERE carne=@CARNE) = 0)
			BEGIN
				SELECT '2'
			END
		ELSE IF((SELECT COUNT(carne) FROM ExamenClinico WHERE carne=@CARNE) = 1)
			BEGIN
				SELECT '3'
			END
		ELSE
			BEGIN
				SELECT '4'
			END

	END













			

